#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(int argc, char** argv) {
  if (argc != 2) {
    printf("Usage: %s <fifo>\n", argv[0]);
    exit(-1);
  }

  mkfifo(argv[1], 0666);

  FILE* f = fopen(argv[1], "r");
  if (f == NULL) {
    perror("Couldn't open fifo");
    exit(-1);
  }

  char* line = NULL;
  size_t line_size = 0;
  while (getline(&line, &line_size, f) >= 0) {
    puts(line);
  }
  fclose(f);
  if (line != NULL) {
    free(line);
  }

  return 0;
}
