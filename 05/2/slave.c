#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char** argv) {
  if (argc != 3) {
    printf("Usage: %s <fifo> <count>\n", argv[0]);
  }

  int count;
  {
    char* end = NULL;
    count = strtol(argv[2], &end, 10);
    if (*end != 0) {
      fprintf(stderr, "The second argument has to be a number\n");
      exit(-1);
    }
  }

  pid_t pid = getpid();

  FILE* f = fopen(argv[1], "w");
  if (f == NULL) {
    perror("Couldn't open fifo");
    exit(-1);
  }

  printf("Pid: %d\n", pid);

  srand(time(NULL));

  for (int i = 0; i < count; i++) {
    FILE* date = popen("date", "r");
    char buff[64] = {};
    fread(buff, 1, sizeof(buff), date);
    pclose(date);
    fprintf(f, "(%d) %s", pid, buff);
    fflush(f);
    sleep(rand() % 4 + 2);
  }

  return 0;
}
