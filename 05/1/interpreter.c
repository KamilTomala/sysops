#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

typedef struct {
  char* cmd;
  size_t arg_count;
  char* args[];
} command;

typedef struct {
  size_t cmd_count;
  command* commands[];
} line;

typedef struct {
  size_t line_count;
  line* lines[];
} file;

command* parse_command(char** src) {
  const size_t START_SIZE = 2;
  command* ret = calloc(1, sizeof(*ret) + START_SIZE * sizeof(ret->args[0]));
  ret->arg_count = START_SIZE;
  char* i = *src;
  while (isspace(*i)) {
    i++;
  }
  char* cmd_start = i;
  while (*i != '|' && *i != 0 && !isspace(*i)) {
    i++;
  }
  ret->cmd = calloc(i - cmd_start + 1, 1);
  memcpy(ret->cmd, cmd_start, i - cmd_start);

  while (isspace(*i)) {
    i++;
  }
  size_t argi = 0;
  while (*i != 0 && *i != '|') {
    char* arg_start = i;
    while (*i != '|' && *i != 0 && !isspace(*i)) {
      i++;
    }
    char* arg = calloc(i - arg_start + 1, 1);
    memcpy(arg, arg_start, i - arg_start);
    if (argi >= ret->arg_count) {
      ret = realloc(ret,
                    sizeof(*ret) + 2 * ret->arg_count * sizeof(ret->args[0]));
      ret->arg_count = 2 * ret->arg_count;
    }
    ret->args[argi] = arg;
    argi++;
    while (isspace(*i)) {
      i++;
    }
  }
  if (*i == '|') {
    i++;
  }
  *src = i;
  ret = realloc(ret, sizeof(*ret) + ret->arg_count * sizeof(ret->args[0]));
  ret->arg_count = argi;
  return ret;
}

line* parse_line(char* src) {
  unsigned pipe_count = 0;
  for (char* i = src; *i != 0; i++) {
    if (*i == '|') {
      pipe_count++;
    }
  }
  line* ret =
      calloc(sizeof(*ret) + (pipe_count + 1) * sizeof(ret->commands[0]), 1);
  ret->cmd_count = pipe_count + 1;

  char* tok = src;

  for (size_t i = 0; i < pipe_count + 1; i++) {
    ret->commands[i] = parse_command(&tok);
  }
  return ret;
}

file* parse_file(char* path) {
  FILE* f = fopen(path, "r");
  if (f == NULL) {
    perror("Couldn't open command list file");
    return NULL;
  }

  const size_t START_SIZE = 8;
  file* ret = calloc(sizeof(*ret) + START_SIZE * sizeof(ret->lines[0]), 1);
  ret->line_count = START_SIZE;

  char* current_line = NULL;
  size_t line_size = 0;

  size_t i = 0;

  while (getline(&current_line, &line_size, f) >= 0) {
    line* l = parse_line(current_line);
    if (l == NULL)
      continue;
    if (i >= ret->line_count) {
      ret = realloc(ret,
                    sizeof(*ret) + ret->line_count * 2 * sizeof(ret->lines[0]));
      ret->line_count *= 2;
    }
    ret->lines[i] = l;
    i++;
  }
  ret = realloc(ret, sizeof(*ret) + i * sizeof(ret->lines[0]));
  ret->line_count = i;
  return ret;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <file>\n", argv[0]);
    exit(-1);
  }

  file* f = parse_file(argv[1]);
  if (f == NULL) {
    exit(-1);
  }

  for (size_t l = 0; l < f->line_count; l++) {
    line* line = f->lines[l];

    pid_t* children = calloc(line->cmd_count, sizeof(*children));

    int prevout = 0;
    int pipefd[2] = {0, 0};
    for (size_t c = 0; c < line->cmd_count; c++) {
      command* cmd = line->commands[c];

      char** args = calloc(cmd->arg_count + 2, sizeof(args[0]));
      args[0] = cmd->cmd;
      memcpy(args + 1, &cmd->args, cmd->arg_count * sizeof(cmd->args[0]));

      if (c < line->cmd_count - 1) {
        pipe(pipefd);
      }

      pid_t child = fork();
      if (child == 0) {
        if (prevout != 0) {
          dup2(prevout, STDIN_FILENO);
        }
        if (c < line->cmd_count - 1) {
          dup2(pipefd[1], STDOUT_FILENO);
          close(pipefd[0]);
        }
        execvp(cmd->cmd, args);
        perror("Couldn't start child process");
        exit(-1);
      } else {
        children[c] = child;
        if (prevout != 0) {
          close(prevout);
        }
        if (c < line->cmd_count - 1) {
          close(pipefd[1]);
          prevout = pipefd[0];
        }
      }
    }
    for (size_t i = 0; i < line->cmd_count; i++) {
      waitpid(children[i], NULL, 0);
    }
    free(children);
  }

  return 0;
}
