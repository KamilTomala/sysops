#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char** argv) {
  if (argc < 3) {
    fprintf(stderr, "Usage: %s <src> <dst>\n", argv[0]);
    exit(-1);
  }
  FILE* src = fopen(argv[1], "r");
  if (src == NULL) {
    perror("Couldn't open source file");
  }
  FILE* dest = fopen(argv[2], "w");
  if (dest == NULL) {
    perror("Couldn't open destination file");
  }

  if (fork() == 0) {
    dup2(fileno(src), STDIN_FILENO);
    dup2(fileno(dest), STDOUT_FILENO);
    execlp("sort", "sort", NULL);

    perror("Couldn't execute sort");
    exit(-1);
  } else {
    wait(NULL);
  }

  return 0;
}
