#include <arpa/inet.h>
#include <poll.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <time.h>
#include <unistd.h>

#include "common.h"

typedef struct timespec timespec;

typedef struct {
  bool present;
  bool busy;
  int fd;
  pthread_mutex_t socket_write_mut;
  timespec last_seen;
  char name[CLIENT_NAME_LENGTH + 1];
} client;

typedef struct {
  pthread_mutex_t mut;
  size_t capacity;
  bool dirty;
  client* clients;
} client_list;

void init_clients(client_list* clients, size_t cap) {
  pthread_mutex_init(&clients->mut, NULL);
  clients->capacity = cap;
  clients->dirty = false;
  clients->clients = calloc(cap, sizeof(client));
  for (size_t i = 0; i < cap; i++) {
    clients->clients[i].present = false;
  }
}

void lock_clients(client_list* clients, const char* msg) {
  int attempts = 5;
  while (pthread_mutex_lock(&clients->mut) != 0) {
    attempts--;
    if (attempts <= 0) {
      fprintf(stderr, "Trying to lock %s:\n", msg);
      perror("Couldn't lock the clients list");
      exit(EXIT_FAILURE);
    }
  }
}

void unlock_clients(client_list* clients) {
  pthread_mutex_unlock(&clients->mut);
}

void lock_client_socket(client* client, const char* msg) {
  int attempts = 5;
  while (pthread_mutex_lock(&client->socket_write_mut) != 0) {
    attempts--;
    if (attempts <= 0) {
      fprintf(stderr, "Trying to lock client socket %s, %s:\n", client->name,
              msg);
      perror("Couldn't lock the client socket");
      exit(EXIT_FAILURE);
    }
  }
}

void unlock_client_socket(client* client) {
  pthread_mutex_unlock(&client->socket_write_mut);
}

void add_client(client_list* clients, int fd) {
  for (size_t i = 0;; i++) {
    if (i >= clients->capacity) {
      size_t new_cap = clients->capacity * 3 / 2;
      clients->clients = realloc(clients->clients, sizeof(client[new_cap]));
      clients->capacity = new_cap;
      for (size_t j = i; j < new_cap; j++) {
        clients->clients[i].present = false;
      }
    }
    if (!clients->clients[i].present) {
      client* c = &clients->clients[i];
      c->present = true;
      c->busy = false;
      c->fd = fd;
      pthread_mutex_init(&c->socket_write_mut, NULL);
      clock_gettime(CLOCK_MONOTONIC, &c->last_seen);
      strcpy(c->name, "???");
      break;
    }
  }
  clients->dirty = true;
}

void remove_client(client_list* clients, client* client) {
  clients->dirty = true;
  client->present = false;
  pthread_mutex_destroy(&client->socket_write_mut);
}

client* get_client(client_list* clients, int fd) {
  for (size_t i = 0; i < clients->capacity; i++) {
    if (clients->clients[i].present && clients->clients[i].fd == fd) {
      return &clients->clients[i];
    }
  }
  return NULL;
}

client* find_free_client(client_list* clients) {
  for (size_t i = 0; i < clients->capacity; i++) {
    client* c = &clients->clients[i];
    if (c->present && !c->busy) {
      return c;
    }
  }
  return NULL;
}

bool check_name_available(client_list* clients, char* name) {
  for (size_t i = 0; i < clients->capacity; i++) {
    if (clients->clients[i].present &&
        strcmp(clients->clients[i].name, name) == 0) {
      return false;
    }
  }
  return true;
}

size_t get_client_count(client_list* clients) {
  size_t ret = 0;
  for (size_t i = 0; i < clients->capacity; i++) {
    if (clients->clients[i].present) {
      ret++;
    }
  }
  return ret;
}

client* get_random_client(client_list* clients) {
  size_t count = get_client_count(clients);
  if (count == 0) {
    return NULL;
  }
  size_t rem = random() % count;
  for (size_t i = 0; i < clients->capacity; i++) {
    client* c = &clients->clients[i];
    if (c->present) {
      if (rem == 0) {
        return c;
      }
      rem--;
    }
  }
  return NULL; // Should never get here
}

void realloc_poll_array(struct pollfd** polls, size_t* count, int tcp_sock,
                        int local_sock, client_list* clients) {
  *count = 2 + get_client_count(clients);
  size_t size = sizeof(struct pollfd[*count]);
  if (*polls == NULL) {
    *polls = malloc(size);
  } else {
    *polls = realloc(*polls, size);
  }
  (*polls)[0] = (struct pollfd){tcp_sock, POLLIN, 0};
  (*polls)[1] = (struct pollfd){local_sock, POLLIN, 0};
  size_t w = 2;
  for (size_t i = 0; i < clients->capacity; i++) {
    if (clients->clients[i].present) {
      (*polls)[w] = (struct pollfd){clients->clients[i].fd, POLLIN, 0};
      w++;
    }
  }
}

typedef struct {
  int tcp_sock, un_sock;
  client_list* clients;
} socket_thread_args;

void* socket_thread(void* va) {
  socket_thread_args* args = va;

  client_list* clients = args->clients;

  struct pollfd* polls = NULL;
  size_t poll_entry_count = 0;
  realloc_poll_array(&polls, &poll_entry_count, args->tcp_sock, args->un_sock,
                     clients);

  while (true) {
    lock_clients(clients, "socket_thread_realloc");
    if (clients->dirty) {
      realloc_poll_array(&polls, &poll_entry_count, args->tcp_sock,
                         args->un_sock, clients);
      clients->dirty = false;
    }
    unlock_clients(clients);
    if (poll(polls, poll_entry_count, -1) < 0) {
      perror("Couldn't poll the sockets");
      exit(EXIT_FAILURE);
    }
    lock_clients(clients, "socket_thread_receive");

    for (size_t i = 0; i < 2; i++) {
      if ((polls[i].revents & POLLIN) != 0) {
        int client_sock = accept(polls[i].fd, NULL, NULL);
        if (client_sock < 0) {
          perror("Couldn't accept client connection");
          continue;
        }
        fprintf(stderr, "Accepted a %s connection %d\n",
                i == 0 ? "tcp" : "unix", client_sock);
        add_client(clients, client_sock);
      }
    }
    for (size_t i = 2; i < poll_entry_count; i++) {
      if ((polls[i].revents & POLLHUP) != 0) {
        client* c = get_client(clients, polls[i].fd);
        printf("Client %s disconnected\n", c->name);
        remove_client(clients, c);
      } else if ((polls[i].revents & POLLIN) != 0) {
        client* c = get_client(clients, polls[i].fd);
        if (c == NULL || !c->present) {
          continue;
        }
        msg_header header;
        if (recv(polls[i].fd, &header, sizeof(header), 0) == 0) {
          printf("Client %s disconnected\n", c->name);
          remove_client(clients, c);
          continue;
        }
        char* body = malloc(header.length + 1);
        body[header.length] = 0;
        if (header.length != 0) {
          if (recv(polls[i].fd, body, header.length, 0) < 0) {
            perror("Couldn't receive from client");
            continue;
          }
        }

        switch (header.type) {
        case CLIENT_HANDSHAKE: {
          if (!check_name_available(clients, body)) {
            fprintf(stderr,
                    "Client %d tried to register with unavailable name %s\n",
                    polls[i].fd, body);
            server_msg_error error_buff = {
                {SERVER_ERROR, MSG_LENGTH(server_msg_error)},
                SERVER_ERR_NAME_TAKEN};

            lock_client_socket(c, "error");
            send(polls[i].fd, &error_buff, sizeof(error_buff), 0);
            unlock_client_socket(c);

            continue;
          }
          client* c = get_client(clients, polls[i].fd);
          if (c != NULL) {
            memcpy(c->name, body, header.length);
            c->name[header.length] = 0;
          }
          printf("Got a handshake from %s\n", c->name);
          break;
        }
        case CLIENT_PONG: {
          client* c = get_client(clients, polls[i].fd);
          if (c == NULL || !c->present) {
            continue;
          }
          clock_gettime(CLOCK_MONOTONIC, &c->last_seen);
          break;
        }
        case CLIENT_RESULT: {
          result_body* res = (void*) body;
          printf("Got result for query %d\n total words: %d\n", res->id,
                 res->word_count);
          printf("Word entries: %d\n", res->entry_count);
          for (size_t i = 0; i < res->entry_count; i++) {
            result_entry* e = &res->entries[i];
            printf("'%16s': %4d\n", e->word, e->count);
          }
          client* c = get_client(clients, polls[i].fd);
          c->busy = false;
          break;
        }
        default:
          fprintf(stderr, "Unknown client message type received: %d\n",
                  header.type);
          break;
        }
        free(body);
      }
    }
    unlock_clients(clients);
  }

  return NULL;
}

typedef struct {
  client_list* clients;
} ping_thread_args;

void* ping_thread(void* va) {
  ping_thread_args* args = va;
  client_list* clients = args->clients;

  msg_header ping_buff = {SERVER_PING, 0};

  while (true) {
    sleep(1);
    lock_clients(clients, "ping_thread");

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    for (size_t i = 0; i < clients->capacity; i++) {
      client* c = &clients->clients[i];
      if (!c->present) {
        continue;
      }
      if (now.tv_sec - c->last_seen.tv_sec >= 5) {
        fprintf(stderr,
                "Client %s (%d) failed to respond for more than 5 seconds, "
                "removing.\n",
                c->name, c->fd);
        remove_client(clients, c);
        continue;
      }

      lock_client_socket(c, "ping");
      send(c->fd, &ping_buff, sizeof(ping_buff), 0);
      unlock_client_socket(c);
    }

    unlock_clients(clients);
  }

  return NULL;
}

typedef struct {
  client_list* clients;
} input_thread_args;

void* input_thread(void* va) {
  input_thread_args* args = va;
  client_list* clients = args->clients;

  uint32_t id = 0;

  char* line = NULL;
  size_t line_size = 0;
  ssize_t line_length = 0;
  while ((line_length = getline(&line, &line_size, stdin)) >= 0) {
    line[line_length - 1] = 0; // Remove the newline char

    lock_clients(clients, "send_task");
    client* c = find_free_client(clients);
    if (c == NULL) {
      c = get_random_client(clients);
    }
    if (c == NULL) {
      fprintf(stderr, "Couldn't find a client to send the task to\n");
      continue;
    }

    c->busy = true;
    unlock_clients(clients);

    struct stat st;
    if (stat(line, &st) != 0) {
      perror("Couldn't get file info");
      lock_clients(clients, "client_reset_busy");
      c->busy = false;
      unlock_clients(clients);
      continue;
    }
    size_t msg_size = sizeof(server_msg_task) + st.st_size;
    server_msg_task* msg = malloc(msg_size);
    msg->header =
        (msg_header){SERVER_TASK, MSG_LENGTH(server_msg_task) + st.st_size};
    id++;
    msg->id = id;
    FILE* f = fopen(line, "r");
    if (f == NULL) {
      perror("Couldn't open the file");
      lock_clients(clients, "client_reset_busy");
      c->busy = false;
      unlock_clients(clients);
      continue;
    }
    fread(msg->body, st.st_size, 1, f);
    fclose(f);

    lock_client_socket(c, "send_task");
    send(c->fd, msg, msg_size, 0);
    unlock_client_socket(c);
    printf("[%u] Sent task for file %s\n", id, line);
  }

  return NULL;
}

int open_tcp_socket(int port) {
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("Couldn't open the tcp socket");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in addr = {
      .sin_family = AF_INET, .sin_port = htons(port), .sin_addr = {INADDR_ANY}};
  if (bind(sock, (struct sockaddr*) &addr, sizeof(addr)) != 0) {
    perror("Couldn't bind the tcp socket");
    exit(EXIT_FAILURE);
  }

  if (listen(sock, 32) != 0) {
    perror("Couldn't listen on the tcp socket");
    exit(EXIT_FAILURE);
  }

  return sock;
}

int open_local_socket(const char* path) {
  int sock = socket(AF_UNIX, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("Couldn't open the local socket");
    exit(EXIT_FAILURE);
  }

  unlink(path);

  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, path);
  if (bind(sock, (struct sockaddr*) &addr, sizeof(addr)) != 0) {
    perror("Couldn't bind the local socket");
    exit(EXIT_FAILURE);
  }

  if (listen(sock, 32) != 0) {
    perror("Couldn't listen on the local socket");
    exit(EXIT_FAILURE);
  }

  return sock;
}

int main(int argc, char** argv) {
  if (argc != 3) {
    printf("Usage: %s <tcp port> <unix socket path>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  char* port_end;
  unsigned long port = strtoul(argv[1], &port_end, 10);
  if (*port_end != 0) {
    fprintf(stderr, "Not a number: '%s'\n", argv[1]);
    exit(EXIT_FAILURE);
  }
  if (port >= 0x10000) {
    fprintf(stderr, "Port too big: %ld\n", port);
    exit(EXIT_FAILURE);
  }

  char* local_socket = argv[2];

  int tcp_sock = open_tcp_socket(port);
  int un_sock = open_local_socket(local_socket);

  client_list clients;
  init_clients(&clients, 4);

  socket_thread_args sargs = {
      .tcp_sock = tcp_sock, .un_sock = un_sock, .clients = &clients};

  ping_thread_args pargs = {&clients};

  input_thread_args iargs = {&clients};

  pthread_t threads[3];

  pthread_create(&threads[0], NULL, socket_thread, &sargs);
  pthread_create(&threads[1], NULL, ping_thread, &pargs);
  pthread_create(&threads[2], NULL, input_thread, &iargs);

  for (size_t i = 0; i < 2; i++) {
    pthread_join(threads[i], NULL);
  }

  return 0;
}
