#include <arpa/inet.h>
#include <ctype.h>
#include <poll.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "common.h"

#define TASK_QUEUE_CAPACITY 16

typedef struct {
  uint32_t id;
  char* contents;
  size_t length;
} task;

void lock(pthread_mutex_t* mut, const char* msg) {
  int attempts = 5;
  while (pthread_mutex_lock(mut) != 0) {
    attempts--;
    if (attempts <= 0) {
      fprintf(stderr, "Trying to lock %s:\n", msg);
      perror("Couldn't lock the mutex");
      exit(EXIT_FAILURE);
    }
  }
}

void unlock(pthread_mutex_t* mut) {
  pthread_mutex_unlock(mut);
}

typedef struct {
  pthread_mutex_t mut;
  pthread_cond_t cond;
  size_t write_i;
  size_t read_i;
  task entries[TASK_QUEUE_CAPACITY];
} task_queue;

void lock_tasks(task_queue* tasks, const char* msg) {
  lock(&tasks->mut, msg);
}

void unlock_tasks(task_queue* tasks) {
  unlock(&tasks->mut);
}

void init_task_queue(task_queue* q) {
  q->write_i = q->read_i = 0;
  pthread_mutex_init(&q->mut, NULL);
  pthread_cond_init(&q->cond, NULL);
}

bool queue_empty(task_queue* q) {
  return q->write_i == q->read_i;
}

task* insert_task(task_queue* q) {
  lock_tasks(q, "insert_task");
  task* ret = &q->entries[q->write_i];
  size_t i = (q->write_i + 1) % TASK_QUEUE_CAPACITY;
  if (i == q->read_i) {
    fprintf(stderr, "Inserting task would overfill the queue\n");
    return NULL;
  }
  q->write_i = i;
  pthread_cond_broadcast(&q->cond);
  unlock_tasks(q);
  return ret;
}

task* peek_task(task_queue* q) {
  lock_tasks(q, "Wait for task");
  while (queue_empty(q)) {
    pthread_cond_wait(&q->cond, &q->mut);
  }
  unlock_tasks(q);
  return &q->entries[q->read_i];
}

void pop_task(task_queue* q) {
  free(q->entries[q->read_i].contents);
  q->entries[q->read_i].contents = NULL;
  q->read_i = (q->read_i + 1) % TASK_QUEUE_CAPACITY;
}

typedef struct map_entry map_entry;

struct map_entry {
  char key[MAX_WORD_LENGTH];
  uint32_t value;
  struct map_entry* next;
};

typedef struct {
  size_t size;
  map_entry* entries[];
} map;

map* alloc_map(size_t size) {
  map* m = malloc(sizeof(*m) + size * sizeof(map_entry*));
  m->size = size;
  for (size_t i = 0; i < size; i++) {
    m->entries[i] = NULL;
  }
  return m;
}

void free_map(map* m) {
  for (size_t i = 0; i < m->size; i++) {
    map_entry* next = m->entries[i];
    while (next != NULL) {
      map_entry* p = next->next;
      free(next);
      next = p;
    }
  }
  free(m);
}

int hash(char* str) {
  int res = 1697;
  for (char* i = str; *i != 0; i++) {
    res *= 787;
    res += *i * 379;
  }
  return res;
}

void map_increment(map* m, char* str) {
  size_t len = strlen(str);
  if (len >= MAX_WORD_LENGTH) {
    return;
  }
  char buff[MAX_WORD_LENGTH];
  strcpy(buff, str);
  for (size_t i = 0; i < len; i++) {
    buff[i] = tolower(buff[i]);
  }
  int h = hash(buff);
  map_entry** entry = &m->entries[h % m->size];
  while (*entry != NULL) {
    if (strcmp((*entry)->key, buff) == 0) {
      (*entry)->value++;
      return;
    }
    entry = &(*entry)->next;
  }
  map_entry* new_entry = malloc(sizeof(*new_entry));
  strcpy(new_entry->key, buff);
  new_entry->value = 1;
  new_entry->next = NULL;
  *entry = new_entry;
}
/*
void debug_print(map* m) {
  for(size_t i = 0; i < m->size; i++) {
    map_entry* p = m->entries[i];
    if(p != NULL) {
      printf("[%4zd] ", i);
      do {
        printf(" -> %s : %d", p->key, p->value);
        p = p->next;
      } while(p != NULL);
      printf("\n");
    }
  }
}
*/

size_t count_entries(map* m) {
  size_t count = 0;
  for (size_t i = 0; i < m->size; i++) {
    for (map_entry* p = m->entries[i]; p != NULL; p = p->next) {
      count++;
    }
  }
  return count;
}

void store_entries(map* m, result_body* body) {
  size_t write_i = 0;
  for (size_t i = 0; i < m->size; i++) {
    for (map_entry* p = m->entries[i]; p != NULL; p = p->next) {
      result_entry* e = &body->entries[write_i];
      e->count = p->value;
      memcpy(e->word, p->key, MAX_WORD_LENGTH);
      write_i++;
    }
  }
}

void print_server_error(server_error err) {
  switch (err) {
  case SERVER_ERR_NAME_TAKEN:
    fprintf(stderr, "Requested name was unavailable\n");
    exit(EXIT_FAILURE);
    break;
  default:
    fprintf(stderr, "Unknown server error code: %d\n", err);
    break;
  }
}

typedef struct {
  int sock;
  task_queue* tasks;
  pthread_mutex_t* socket_mut;
} socket_thread_args;

void* socket_thread(void* va) {
  socket_thread_args* args = va;
  int sock = args->sock;
  pthread_mutex_t* socket_mut = args->socket_mut;
  task_queue* tasks = args->tasks;

  struct pollfd polls = {args->sock, POLLIN, 0};

  while (true) {
    if (poll(&polls, 1, -1) < 0) {
      perror("Couldn't poll the socket");
      exit(EXIT_FAILURE);
    }
    if ((polls.revents & POLLIN) != 0) {
      msg_header header;
      if (recv(sock, &header, sizeof(header), 0) == 0) {
        printf("Server disconnected\n");
        exit(EXIT_FAILURE);
      }
      char* body = malloc(header.length + 1);
      body[header.length] = 0;
      if (recv(sock, body, header.length, 0) < 0) {
        perror("Couldn't receive from server");
        continue;
      }
      switch (header.type) {
      case SERVER_ERROR:
        if (header.length < 1) {
          fprintf(stderr,
                  "Server reported an error, but no error code was provided");
          break;
        }
        print_server_error(body[0]);
        break;
      case SERVER_PING: {
        msg_header pong_buff = {CLIENT_PONG, 0};
        lock(socket_mut, "pong");
        send(sock, &pong_buff, sizeof(pong_buff), 0);
        unlock(socket_mut);
        break;
      }
      case SERVER_TASK: {
        struct {
          uint32_t id;
          char body[];
        }* data = (void*) body;
        printf("received a task from server %d\n", data->id);
        task* t = insert_task(tasks);
        t->id = data->id;
        t->length = header.length - sizeof(uint32_t);
        t->contents = malloc(header.length);
        memcpy(t->contents, data->body, header.length - sizeof(uint32_t));
        break;
      }
      default:
        fprintf(stderr, "Unknown message type: %d\n", header.type);
      }
      free(body);
    }
  }

  return NULL;
}

typedef struct {
  int count;
  map* words;
} count_res;

count_res count_words(task* t) {
  map* words = alloc_map(100);

  char* save;
  char* delims = " \t\r\n,.!?-()[];:><&\"'*/+|";
  char* tok = strtok_r(t->contents, delims, &save);

  size_t count = 0;

  while (tok != NULL) {
    map_increment(words, tok);
    count++;
    tok = strtok_r(NULL, delims, &save);
  }

  return (count_res){count, words};
}

typedef struct {
  int sock;
  task_queue* tasks;
  pthread_mutex_t* socket_mut;
} work_thread_args;

void* work_thread(void* va) {
  work_thread_args* args = va;
  int sock = args->sock;
  pthread_mutex_t* socket_mut = args->socket_mut;
  task_queue* tasks = args->tasks;

  while (true) {
    task* t = peek_task((task_queue*) tasks);

    count_res res = count_words(t);

    size_t entry_count = count_entries(res.words);

    size_t len =
        MSG_LENGTH(client_msg_result) + sizeof(result_entry[entry_count]);
    client_msg_result* msg = malloc(sizeof(msg_header) + len);
    msg->header = (msg_header){CLIENT_RESULT, len};
    msg->body.id = t->id;
    msg->body.word_count = res.count;
    msg->body.entry_count = entry_count;
    store_entries(res.words, &msg->body);
    free_map(res.words);

    lock(socket_mut, "send_result");
    send(sock, msg, sizeof(msg_header) + len, 0);
    unlock(socket_mut);

    free(msg);

    pop_task((task_queue*) tasks);
  }
  return NULL;
}

void send_handshake(int sock, char* name) {
  size_t len = strlen(name);
  client_msg_handshake buff = {{CLIENT_HANDSHAKE, len}, {}};

  memcpy(buff.name, name, len);
  if (send(sock, &buff, sizeof(msg_header) + len, 0) < 0) {
    perror("Couldn't send the handshake");
    exit(EXIT_FAILURE);
  }
  fprintf(stderr, "Sent handshake %d %zu %s\n", CLIENT_HANDSHAKE, len, name);
}

int main(int argc, char** argv) {
  if (argc < 4) {
    printf("Usage: %s <client name> <inet|unix> <server address> [port]\n",
           argv[0]);
    exit(EXIT_FAILURE);
  }

  if (strlen(argv[1]) >= CLIENT_NAME_LENGTH) {
    fprintf(stderr, "Client name too long. Max length is %d.\n",
            CLIENT_NAME_LENGTH);
    exit(EXIT_FAILURE);
  }

  int sock;

  if (strcasecmp(argv[2], "inet") == 0) {
    if (argc != 5) {
      fprintf(stderr, "For inet connection port must be specified.\n");
      exit(EXIT_FAILURE);
    }

    struct sockaddr_in addr_in;

    if (inet_aton(argv[3], &addr_in.sin_addr) == 0) {
      fprintf(stderr, "Invalid ip address '%s'.\n", argv[3]);
      exit(EXIT_FAILURE);
    }

    char* port_end;
    long port = strtol(argv[4], &port_end, 10);
    if (*port_end != 0) {
      fprintf(stderr, "Not a number: '%s'\n", argv[4]);
      exit(EXIT_FAILURE);
    }
    if (port >= 0x10000) {
      fprintf(stderr, "Port too big\n");
      exit(EXIT_FAILURE);
    }
    addr_in.sin_port = htons(port);
    addr_in.sin_family = AF_INET;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    connect(sock, (struct sockaddr*) &addr_in, sizeof(addr_in));

  } else if (strcasecmp(argv[2], "unix") == 0) {
    struct sockaddr_un addr_un;
    addr_un.sun_family = AF_UNIX;
    strncpy(addr_un.sun_path, argv[3], sizeof(addr_un.sun_path));

    sock = socket(AF_UNIX, SOCK_STREAM, 0);
    connect(sock, (struct sockaddr*) &addr_un, sizeof(addr_un));
  } else {
    fprintf(
        stderr,
        "Wrong connection type '%s'. Only 'inet' and 'unix' are supported.\n",
        argv[2]);
    exit(EXIT_FAILURE);
  }

  send_handshake(sock, argv[1]);

  pthread_t sock_thread, worker_thread;
  task_queue tasks;
  init_task_queue(&tasks);
  pthread_mutex_t socket_mutex;
  pthread_mutex_init(&socket_mutex, NULL);
  socket_thread_args sargs = {sock, &tasks, &socket_mutex};
  work_thread_args wargs = {sock, &tasks, &socket_mutex};
  pthread_create(&sock_thread, NULL, socket_thread, &sargs);
  pthread_create(&worker_thread, NULL, work_thread, &wargs);

  pthread_join(sock_thread, NULL);
  pthread_join(worker_thread, NULL);

  return EXIT_SUCCESS;
}
