#include <stdint.h>
#include <stdlib.h>

#define CLIENT_NAME_LENGTH 16
#define MAX_WORD_LENGTH 16

enum client_msg_type { CLIENT_HANDSHAKE, CLIENT_PONG, CLIENT_RESULT };

enum server_msg_type { SERVER_ERROR, SERVER_PING, SERVER_TASK };

enum server_error { SERVER_ERR_NAME_TAKEN };

typedef enum client_msg_type client_msg_type;
typedef enum server_msg_type server_msg_type;
typedef enum server_error server_error;

typedef struct {
  unsigned char type;
  uint32_t length;
} msg_header;

typedef struct {
  msg_header header;
  server_error error;
} server_msg_error;

typedef struct {
  msg_header header;
  uint32_t id;
  char body[];
} server_msg_task;

typedef struct {
  msg_header header;
  char name[CLIENT_NAME_LENGTH];
} client_msg_handshake;

typedef struct {
  char word[MAX_WORD_LENGTH];
  uint32_t count;
} result_entry;

typedef struct {
  uint32_t id;
  uint32_t word_count;
  uint32_t entry_count;
  result_entry entries[];
} result_body;

typedef struct {
  msg_header header;
  result_body body;
} client_msg_result;

#define MSG_LENGTH(type) (sizeof(type) - sizeof(msg_header))
