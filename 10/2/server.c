#include <arpa/inet.h>
#include <poll.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <time.h>
#include <unistd.h>

#include "common.h"

#undef unix

typedef struct timespec timespec;

typedef union {
    struct sockaddr_in inet;
    struct sockaddr_un unix;
} socket_addr;

typedef struct {
  bool present;
  bool busy;
  socklen_t sock_len;
  socket_addr sock;
  int s_sock;
  pthread_mutex_t socket_write_mut;
  timespec last_seen;
  char name[CLIENT_NAME_LENGTH + 1];
  chunk_state pending_chunks;
} client;

typedef struct {
  pthread_mutex_t mut;
  size_t capacity;
  bool dirty;
  client* clients;
} client_list;

void init_clients(client_list* clients, size_t cap) {
  pthread_mutex_init(&clients->mut, NULL);
  clients->capacity = cap;
  clients->dirty = false;
  clients->clients = calloc(cap, sizeof(client));
  for (size_t i = 0; i < cap; i++) {
    clients->clients[i].present = false;
  }
}

void lock_clients(client_list* clients, const char* msg) {
  int attempts = 5;
  while (pthread_mutex_lock(&clients->mut) != 0) {
    attempts--;
    if (attempts <= 0) {
      fprintf(stderr, "Trying to lock %s:\n", msg);
      perror("Couldn't lock the clients list");
      exit(EXIT_FAILURE);
    }
  }
}

void unlock_clients(client_list* clients) {
  pthread_mutex_unlock(&clients->mut);
}

void lock_client_socket(client* client, const char* msg) {
  int attempts = 5;
  while (pthread_mutex_lock(&client->socket_write_mut) != 0) {
    attempts--;
    if (attempts <= 0) {
      fprintf(stderr, "Trying to lock client socket %s, %s:\n", client->name,
              msg);
      perror("Couldn't lock the client socket");
      exit(EXIT_FAILURE);
    }
  }
}

void unlock_client_socket(client* client) {
  pthread_mutex_unlock(&client->socket_write_mut);
}

void add_client(client_list* clients, socklen_t sock_len, socket_addr* addr, char* name, int s_sock) {
  for (size_t i = 0;; i++) {
    if (i >= clients->capacity) {
      size_t new_cap = clients->capacity * 3 / 2;
      clients->clients = realloc(clients->clients, sizeof(client[new_cap]));
      clients->capacity = new_cap;
      for (size_t j = i; j < new_cap; j++) {
        clients->clients[i].present = false;
      }
    }
    if (!clients->clients[i].present) {
      client* c = &clients->clients[i];
      c->present = true;
      c->busy = false;
      c->sock_len = sock_len;
      c->sock = *addr;
      c->s_sock = s_sock;
      init_chunk_state(&c->pending_chunks);
      pthread_mutex_init(&c->socket_write_mut, NULL);
      clock_gettime(CLOCK_MONOTONIC, &c->last_seen);
      strncpy(c->name, name, CLIENT_NAME_LENGTH);
      break;
    }
  }
  clients->dirty = true;
}

void remove_client(client_list* clients, client* client) {
  clients->dirty = true;
  client->present = false;
  pthread_mutex_destroy(&client->socket_write_mut);
}

client* get_client(client_list* clients, char* name) {
  for (size_t i = 0; i < clients->capacity; i++) {
    if (clients->clients[i].present && strcmp(clients->clients[i].name, name) == 0) {
      return &clients->clients[i];
    }
  }
  return NULL;
}

client* find_free_client(client_list* clients) {
  for (size_t i = 0; i < clients->capacity; i++) {
    client* c = &clients->clients[i];
    if (c->present && !c->busy) {
      return c;
    }
  }
  return NULL;
}

bool check_name_available(client_list* clients, char* name) {
  for (size_t i = 0; i < clients->capacity; i++) {
    if (clients->clients[i].present &&
        strcmp(clients->clients[i].name, name) == 0) {
      return false;
    }
  }
  return true;
}

size_t get_client_count(client_list* clients) {
  size_t ret = 0;
  for (size_t i = 0; i < clients->capacity; i++) {
    if (clients->clients[i].present) {
      ret++;
    }
  }
  return ret;
}

client* get_random_client(client_list* clients) {
  size_t count = get_client_count(clients);
  if (count == 0) {
    return NULL;
  }
  size_t rem = random() % count;
  for (size_t i = 0; i < clients->capacity; i++) {
    client* c = &clients->clients[i];
    if (c->present) {
      if (rem == 0) {
        return c;
      }
      rem--;
    }
  }
  return NULL; // Should never get here
}

/*void realloc_poll_array(struct pollfd** polls, size_t* count, int udp_sock,
                        int local_sock, client_list* clients) {
  *count = 2 + get_client_count(clients);
  size_t size = sizeof(struct pollfd[*count]);
  if (*polls == NULL) {
    *polls = malloc(size);
  } else {
    *polls = realloc(*polls, size);
  }
  (*polls)[0] = (struct pollfd){udp_sock, POLLIN, 0};
  (*polls)[1] = (struct pollfd){local_sock, POLLIN, 0};
  size_t w = 2;
  for (size_t i = 0; i < clients->capacity; i++) {
    if (clients->clients[i].present) {
      (*polls)[w] = (struct pollfd){clients->clients[i].fd, POLLIN, 0};
      w++;
    }
  }
}*/

typedef struct {
  int udp_sock, un_sock;
  client_list* clients;
} socket_thread_args;

void* socket_thread(void* va) {
  socket_thread_args* args = va;

  client_list* clients = args->clients;

  struct pollfd polls[] = {{args->udp_sock, POLLIN, 0}, {args->un_sock, POLLIN, 0}};

  const size_t msg_buffer_size = 1 << 16;
  void* msg_buffer = malloc(msg_buffer_size);
  struct sockaddr* client_addr = malloc(sizeof(socket_addr));

  while (true) {
    if (poll(polls, 2, -1) < 0) {
      perror("Couldn't poll the sockets");
      exit(EXIT_FAILURE);
    }
    lock_clients(clients, "socket_thread_receive");

    for (size_t i = 0; i < 2; i++) {
      if ((polls[i].revents & POLLIN) != 0) {
        int sock = polls[i].fd;
        socklen_t addr_len = sizeof(socket_addr);
        ssize_t msg_size = recvfrom(sock, msg_buffer, msg_buffer_size, 0, client_addr, &addr_len);
        if(msg_size < 0) {
          perror("Couldn't receive message");
          continue;
        }
        if (msg_size < sizeof(client_msg_header)) {
          fprintf(stderr, "Message too short: %zu\n", msg_size);
          continue;
        }
        client_msg_header* header = (client_msg_header*) msg_buffer;
        switch(header->type) {
        case CLIENT_HANDSHAKE: {
          if (!check_name_available(clients, header->name)) {
            fprintf(stderr,
                    "Client %d tried to register with unavailable name %s\n",
                    polls[i].fd, header->name);
            server_msg_error error_buff = {SERVER_ERROR, SERVER_ERR_NAME_TAKEN};

            sendto(sock, &error_buff, sizeof(error_buff), 0, client_addr, addr_len);
            continue;
          }
          add_client(clients, addr_len, (socket_addr*) client_addr, header->name, sock);

          printf("Got a handshake from %s\n", header->name);
          break;
        }
        case CLIENT_PONG: {
          client* c = get_client(clients, header->name);
          if (c == NULL || !c->present) {
            continue;
          }
          clock_gettime(CLOCK_MONOTONIC, &c->last_seen);
          break;
        }
        case CLIENT_RESULT: {
          client* c = get_client(clients, header->name);
          client_msg_result* result_msg = msg_buffer;
          insert_chunk(&c->pending_chunks, &result_msg->chunk);
          result_body* res = check_chunk_done(&c->pending_chunks);
          if (res != NULL) {
            printf("Got result for query %d\n total words: %d\n", res->id,
                   res->word_count);
            printf("Word entries: %d\n", res->entry_count);
            for (size_t i = 0; i < res->entry_count; i++) {
              result_entry* e = &res->entries[i];
              printf("'%16s': %4d\n", e->word, e->count);
            }
            c->busy = false;
          }
          break;
        }
        default:
          fprintf(stderr, "Unknown client message type received: %d\n",
                  header->type);
          break;
        }
      }
    }
    unlock_clients(clients);
  }

  return NULL;
}

typedef struct {
  client_list* clients;
} ping_thread_args;

void* ping_thread(void* va) {
  ping_thread_args* args = va;
  client_list* clients = args->clients;

  unsigned char ping_buff = SERVER_PING;
  unsigned char disconnect_buff = SERVER_DISCONNECT;

  while (true) {
    sleep(1);
    lock_clients(clients, "ping_thread");

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    for (size_t i = 0; i < clients->capacity; i++) {
      client* c = &clients->clients[i];
      if (!c->present) {
        continue;
      }
      if (now.tv_sec - c->last_seen.tv_sec >= 5) {
        fprintf(stderr,
                "Client %s failed to respond for more than 5 seconds, "
                "removing.\n",
                c->name);
        lock_client_socket(c, "ping timeout");
        sendto(c->s_sock, &disconnect_buff, sizeof(disconnect_buff), 0, (struct sockaddr*) &c->sock, c->sock_len);
        unlock_client_socket(c);
        remove_client(clients, c);
        continue;
      }

      lock_client_socket(c, "ping");
      sendto(c->s_sock, &ping_buff, sizeof(ping_buff), 0, (struct sockaddr*) &c->sock, c->sock_len);
      unlock_client_socket(c);
    }

    unlock_clients(clients);
  }

  return NULL;
}

typedef struct {
  client_list* clients;
} input_thread_args;

void* input_thread(void* va) {
  input_thread_args* args = va;
  client_list* clients = args->clients;

  uint32_t id = 0;

  server_msg_task* msg = malloc(sizeof(server_msg_task));
  msg->type = SERVER_TASK;

  char* line = NULL;
  size_t line_size = 0;
  ssize_t line_length = 0;
  while ((line_length = getline(&line, &line_size, stdin)) >= 0) {
    line[line_length - 1] = 0; // Remove the newline char

    lock_clients(clients, "send_task");
    client* c = find_free_client(clients);
    if (c == NULL) {
      c = get_random_client(clients);
    }
    if (c == NULL) {
      fprintf(stderr, "Couldn't find a client to send the task to\n");
      continue;
    }

    c->busy = true;
    unlock_clients(clients);

    struct stat st;
    if (stat(line, &st) != 0) {
      perror("Couldn't get file info");
      lock_clients(clients, "client_reset_busy");
      c->busy = false;
      unlock_clients(clients);
      continue;
    }
    /* size_t msg_size = sizeof(server_msg_task) + st.st_size; */
    id++;
    msg->id = id;

    FILE* f = fopen(line, "r");
    if (f == NULL) {
      perror("Couldn't open the file");
      lock_clients(clients, "client_reset_busy");
      c->busy = false;
      unlock_clients(clients);
      continue;
    }
    void* contents = malloc(st.st_size + 1);
    fread(contents, st.st_size, 1, f);
    fclose(f);

    lock_client_socket(c, "send_task");
    for(send_iterator* i = prepare_send_chunk(contents, st.st_size); send_chunk_has_next(i);) {
      size_t chunk_size = send_next_chunk(i, &msg->chunk);
      size_t msg_size = sizeof(*msg) - sizeof(msg->chunk.data) + chunk_size;
      sendto(c->s_sock, msg, msg_size, 0, (struct sockaddr*) &c->sock, c->sock_len);
    }
    unlock_client_socket(c);
    printf("[%u] Sent task for file %s\n", id, line);
  }

  return NULL;
}

int open_udp_socket(int port) {
  int sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0) {
    perror("Couldn't open the udp socket");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in addr = {
      .sin_family = AF_INET, .sin_port = htons(port), .sin_addr = {INADDR_ANY}};
  if (bind(sock, (struct sockaddr*) &addr, sizeof(addr)) != 0) {
    perror("Couldn't bind the udp socket");
    exit(EXIT_FAILURE);
  }

  return sock;
}

int open_local_socket(const char* path) {
  int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
  if (sock < 0) {
    perror("Couldn't open the local socket");
    exit(EXIT_FAILURE);
  }

  unlink(path);

  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, path);
  if (bind(sock, (struct sockaddr*) &addr, sizeof(addr)) != 0) {
    perror("Couldn't bind the local socket");
    exit(EXIT_FAILURE);
  }

  return sock;
}

int main(int argc, char** argv) {
  if (argc != 3) {
    printf("Usage: %s <udp port> <unix socket path>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  char* port_end;
  unsigned long port = strtoul(argv[1], &port_end, 10);
  if (*port_end != 0) {
    fprintf(stderr, "Not a number: '%s'\n", argv[1]);
    exit(EXIT_FAILURE);
  }
  if (port >= 0x10000) {
    fprintf(stderr, "Port too big: %ld\n", port);
    exit(EXIT_FAILURE);
  }

  char* local_socket = argv[2];

  int udp_sock = open_udp_socket(port);
  int un_sock = open_local_socket(local_socket);

  client_list clients;
  init_clients(&clients, 4);

  socket_thread_args sargs = {
      .udp_sock = udp_sock, .un_sock = un_sock, .clients = &clients};

  ping_thread_args pargs = {&clients};

  input_thread_args iargs = {&clients};

  pthread_t threads[3];

  pthread_create(&threads[0], NULL, socket_thread, &sargs);
  pthread_create(&threads[1], NULL, ping_thread, &pargs);
  pthread_create(&threads[2], NULL, input_thread, &iargs);

  for (size_t i = 0; i < 2; i++) {
    pthread_join(threads[i], NULL);
  }

  return 0;
}
