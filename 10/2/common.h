#include <stdint.h>
#include <stdlib.h>

#include "chunk.h"

#define CLIENT_NAME_LENGTH 16
#define MAX_WORD_LENGTH 16

enum client_msg_type { CLIENT_HANDSHAKE, CLIENT_PONG, CLIENT_RESULT };

enum server_msg_type { SERVER_ERROR, SERVER_PING, SERVER_TASK, SERVER_DISCONNECT };

enum server_error { SERVER_ERR_NAME_TAKEN };

typedef enum client_msg_type client_msg_type;
typedef enum server_msg_type server_msg_type;
typedef enum server_error server_error;

typedef struct {
  unsigned char type;
  char name[CLIENT_NAME_LENGTH];
} client_msg_header;

typedef struct {
  unsigned char type;
  server_error error;
} server_msg_error;

typedef struct {
  unsigned char type;
  uint32_t id;
  chunk_msg chunk;
} server_msg_task;

typedef struct {
  char word[MAX_WORD_LENGTH];
  uint32_t count;
} result_entry;

typedef struct {
  uint32_t id;
  uint32_t word_count;
  uint32_t entry_count;
  result_entry entries[];
} result_body;

typedef struct {
  client_msg_header header;
  chunk_msg chunk;
} client_msg_result;

#define MSG_LENGTH(type) (sizeof(type) - sizeof(msg_header))
