#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_CHUNK_SIZE 8192

typedef struct {
  size_t msg_length;
  size_t received;
  uint8_t* buffer;
} chunk_state;

typedef struct {
  uint32_t length;
  uint32_t offset;
  uint8_t data[MAX_CHUNK_SIZE];
} chunk_msg;

typedef struct send_iterator send_iterator;

void init_chunk_state(chunk_state* state);

void insert_chunk(chunk_state* state, chunk_msg* msg);

void* check_chunk_done(chunk_state* state);

send_iterator* prepare_send_chunk(void* buffer, size_t size);

size_t send_next_chunk(send_iterator* iter, chunk_msg* msg);

bool send_chunk_has_next(send_iterator* iter);
