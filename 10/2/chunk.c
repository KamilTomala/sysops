#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "chunk.h"

struct send_iterator {
  void* buffer;
  size_t length;
  size_t sent;
};

void init_chunk_state(chunk_state* state) {
  state->buffer = NULL;
}

void insert_chunk(chunk_state* state, chunk_msg* msg) {
  if (state->buffer == NULL) {
    state->msg_length = msg->length;
    state->received = 0;
    state->buffer = malloc(msg->length + 1);
    state->buffer[msg->length] = 0;
  }
  if (state->msg_length != msg->length) {
    fprintf(stderr, "Message size mismatch, old: %zu, new: %u", state->msg_length, msg->length);
    exit(EXIT_FAILURE);
  }
  size_t rem = msg->length - msg->offset;
  size_t size = rem > MAX_CHUNK_SIZE ? MAX_CHUNK_SIZE : rem;
  state->received += size;
  memcpy(state->buffer + msg->offset, msg->data, size);
}

void* check_chunk_done(chunk_state* state) {
  if(state->received == state->msg_length) {
    void* ret = state->buffer;
    state->buffer = NULL;
    return ret;
  }
  return NULL;
}

send_iterator* prepare_send_chunk(void* buffer, size_t size) {
  send_iterator* iter = malloc(sizeof(*iter));
  iter->buffer = buffer;
  iter->length = size;
  iter->sent = 0;
  return iter;
}

size_t send_next_chunk(send_iterator* iter, chunk_msg* msg) {
  size_t size = (iter->length - iter->sent) >= MAX_CHUNK_SIZE ? MAX_CHUNK_SIZE : iter->length - iter->sent;
  memcpy(msg->data, iter->buffer + iter->sent, size);
  msg->length = iter->length;
  msg->offset = iter->sent;
  iter->sent += size;
  return size;
}

bool send_chunk_has_next(send_iterator* iter) {
  return iter->sent < iter->length;
}
