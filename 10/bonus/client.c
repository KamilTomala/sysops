#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include "common.h"

int main(int argc, char** argv) {
  int r_sock = socket(AF_LOCAL, SOCK_STREAM, 0);
  char saddr[64];
  sprintf(saddr, "%s/" SOCK_NAME, getenv("HOME"));
  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, saddr);
  /* bind(r_sock, (struct sockaddr*) &addr, sizeof(addr));   */
  connect(r_sock, (struct sockaddr*) &addr, sizeof(addr));
  char ping[32] = {0};
  char pong[] = "pong";
  while(true) {
    while(recv(r_sock, ping, sizeof(ping), 0) == 0);
    printf("%s\n", ping);
    sleep(1);
    send(r_sock, pong, sizeof(pong), 0);
  }
  return 0;
}
