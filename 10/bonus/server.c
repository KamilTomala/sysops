#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include "common.h"

volatile bool running = true;

void on_term(int sig) {
  running = false;
}

void* ping_thread(void* args) {
  int sock = *((int*) args);
  char ping[] = "ping";
  while(running) {
    sleep(2);
    send(sock, &ping, sizeof(ping), 0);
  }
  return NULL;
}

int main(int argc, char** argv) {

  sigaction(SIGTERM, &(struct sigaction) {.sa_handler = on_term, .sa_flags = 0}, NULL);

  int sock = socket(AF_LOCAL, SOCK_STREAM, 0);
  if(sock < 0) {
    perror("socket");
    exit(EXIT_FAILURE);
  }


  char saddr[64];
  sprintf(saddr, "%s/" SOCK_NAME, getenv("HOME"));
  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, saddr);
  if(bind(sock, (struct sockaddr*) &addr, sizeof(addr)) != 0) {
    perror("bind");
    exit(EXIT_FAILURE);
  }
  if(listen(sock, 2) != 0) {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  /* struct sockaddr_un remote; */
  int r_sock = accept(sock, NULL, NULL);
  printf("%d\n", r_sock);
  if(r_sock < 0) {
    perror("accept");
    exit(EXIT_FAILURE);
  }

  pthread_t thread;
  pthread_create(&thread, NULL, ping_thread, &r_sock);

  while(running) {
    char buff[32];
    while(recv(r_sock, buff, sizeof(buff), 0) <= 0) {
      /* perror("recv"); */
    }
    printf("%s\n", buff);
  }

  pthread_join(thread, NULL);

  return 0;
}
