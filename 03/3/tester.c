#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char** argv) {
  if (argc < 5) {
    fprintf(stderr, "Too few arguments.\n");
  }
  long pmin, pmax;
  size_t bytes;
  {
    char* end;
    pmin = strtol(argv[2], &end, 10);
    if (*end != 0) {
      fprintf(stderr, "The second argument has to be a number\n");
      return -1;
    }
  }
  {
    char* end;
    pmax = strtol(argv[3], &end, 10);
    if (*end != 0) {
      fprintf(stderr, "The third argument has to be a number\n");
      return -1;
    }
  }
  {
    char* end;
    bytes = strtoul(argv[4], &end, 10);
    if (*end != 0) {
      fprintf(stderr, "The fourth argument has to be a number\n");
      return -1;
    }
  }

  char* random_str = calloc(1, bytes + 1);
  while (true) {
    int wait_time = rand() % (pmax - pmin + 1) + pmin;
    sleep(wait_time);

    const size_t NOW_LENGTH = 20;
    char now_str[NOW_LENGTH];
    time_t now = time(NULL);
    strftime(now_str, NOW_LENGTH, "%Y-%m-%d_%H-%M-%S", localtime(&now));

    for (size_t i = 0; i < bytes; i++) {
      random_str[i] = 'a' + rand() % ('z' - 'a' + 1);
    }

    FILE* f = fopen(argv[1], "a");
    fprintf(f, "%d %d %s - %s\n", getpid(), wait_time, now_str, random_str);
    fclose(f);
  }

  free(random_str);
  return 0;
}
