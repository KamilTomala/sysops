#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>

typedef struct {
  char* path;
  int interval;
} file_entry;

typedef struct {
  size_t size;
  file_entry entries[];
} file_array;

typedef enum { MODE_CP, MODE_MEM } mode;

typedef struct {
  char* buffer;
  size_t allocated;
  size_t used;
} mem_buffer;

size_t count_lines(FILE* f) {
  size_t lines = 0;
  char c;
  bool newline_last = false;
  while ((c = fgetc(f)) != EOF) {
    if (c == '\n') {
      lines++;
      newline_last = true;
    } else {
      newline_last = false;
    }
  }
  if (!newline_last) {
    lines++;
  }
  return lines;
}

file_array* alloc_file_array(size_t size) {
  file_array* ret = calloc(sizeof(file_array) + size * sizeof(file_entry), 1);
  ret->size = size;
  return ret;
}

void parse_line(char* line, file_entry* entry) {
  size_t i = 0;
  // Skip any whitespace at the beginning
  while (isspace(line[i])) {
    i++;
  }

  size_t path_start;
  size_t path_end;

  if (line[i] == '"') {
    i++;
    path_start = i;
    while (line[i] != '"') {
      i++;
    }
    path_end = i;
    i++;
  } else {
    path_start = i;
    while (!isspace(line[i])) {
      i++;
    }
    path_end = i;
  }

  char* path = calloc(1, path_end - path_start + 1);
  memcpy(path, line + path_start, path_end - path_start);

  while (isspace(line[i]))
    i++;

  int interval;
  sscanf(line + i, "%u", &interval);

  entry->path = path;
  entry->interval = interval;
}

file_array* parse_file(char* path) {
  FILE* f = fopen(path, "r");
  if (f == NULL) {
    perror("Couldn't open the list file");
    return NULL;
  }
  size_t lines = count_lines(f);

  file_array* arr = alloc_file_array(lines);

  char* line = NULL;
  size_t line_size = 0;

  fseek(f, 0, SEEK_SET);
  for (size_t i = 0; i < lines; i++) {
    getline(&line, &line_size, f);
    parse_line(line, &arr->entries[i]);
  }
  if (line != NULL) {
    free(line);
  }

  return arr;
}

time_t get_modification_date(char* path) {
  struct stat res;
  lstat(path, &res);
  return res.st_mtim.tv_sec;
}

char* make_backup_path(char* path, time_t mod) {
  const char* const prefix = "archiwum/";
  const size_t prefix_size = strlen(prefix);
  size_t len = strlen(path);

  size_t date_size = 21;
  char* date_str = calloc(1, date_size);

  struct tm* t = localtime(&mod);
  strftime(date_str, date_size, "_%Y-%m-%d_%H-%M-%S", t);

  size_t ep_len = len + prefix_size + date_size + 1;
  char* escaped_path = calloc(1, ep_len);
  snprintf(escaped_path, ep_len, "%s%s%s", prefix, path, date_str);
  for (size_t i = 0; i < len; i++) {
    if (escaped_path[prefix_size + i] == '/') {
      escaped_path[prefix_size + i] = '!';
    }
  }
  return escaped_path;
}

void backup_copy(char* path, time_t mod) {
  pid_t child = fork();
  if (child == 0) {
    char* escaped_path = make_backup_path(path, mod);
    execlp("cp", "cp", path, escaped_path, NULL);
  } else {
    waitpid(child, NULL, 0);
  }
}

void read_to_mem(char* path, mem_buffer* buffer) {
  FILE* f = fopen(path, "r");
  fseek(f, 0, SEEK_END);
  size_t fsize = ftell(f);
  fseek(f, 0, SEEK_SET);

  if (buffer->buffer == NULL) {
    buffer->buffer = malloc(fsize);
    buffer->allocated = fsize;
  } else if (buffer->allocated < fsize) {
    buffer->buffer = realloc(buffer->buffer, fsize);
    buffer->allocated = fsize;
  }

  if (fsize != 0 && buffer->buffer == NULL) {
    perror("Couldn't allocate buffer for file");
    exit(-1);
  }

  fread(buffer->buffer, 1, fsize, f);
  buffer->used = fsize;

  fclose(f);
}

void backup_mem(char* path, time_t mod, mem_buffer* buffer) {
  char* escaped_path = make_backup_path(path, mod);

  FILE* f = fopen(escaped_path, "w");
  fwrite(buffer->buffer, 1, buffer->used, f);
  fclose(f);

  read_to_mem(path, buffer);
}


void handle_xcpu(int sig) {
  printf("Cpu time limit exceeded!\n");
  exit(-1);
}

void monitor(file_entry* entry, int duration, mode m) {
  fprintf(stderr, "%s %d %d\n", entry->path, entry->interval, duration);

  signal(SIGXCPU, handle_xcpu);

  struct timeval start_time;
  gettimeofday(&start_time, NULL);

  time_t last_modified = get_modification_date(entry->path);

  int copies_made = 0;
  mem_buffer buffer = {.buffer = NULL, .allocated = 0, .used = 0};

  switch (m) {
  case MODE_CP:
    backup_copy(entry->path, last_modified);
    break;
  case MODE_MEM:
    read_to_mem(entry->path, &buffer);
    break;
  }

  while (true) {
    time_t mod = get_modification_date(entry->path);
    if (mod > last_modified) {
      last_modified = mod;
      switch (m) {
      case MODE_CP:
        backup_copy(entry->path, last_modified);
        break;
      case MODE_MEM:
        backup_mem(entry->path, last_modified, &buffer);
        break;
      }
      copies_made++;
      printf("File %s modified\n", entry->path);
    }

    sleep(entry->interval);

    struct timeval now;
    gettimeofday(&now, NULL);
    if (now.tv_sec - start_time.tv_sec + entry->interval >= duration) {
      struct rusage usage;
      getrusage(RUSAGE_SELF, &usage);
      pid_t pid = getpid();
      printf("Pid %d usage: sys %lu us user %lu us\n", pid,
             usage.ru_stime.tv_usec + usage.ru_stime.tv_sec * 1000000,
             usage.ru_utime.tv_usec + usage.ru_utime.tv_sec * 1000000);
      exit(copies_made);
    }
  }
}

int main(int argc, char** argv) {
  if (argc < 6) {
    fprintf(stderr, "Too few arguments\n");
    return -1;
  }

  int duration;
  {
    char* end;
    duration = strtol(argv[2], &end, 10);
    if (*end != 0) {
      fprintf(stderr, "The second argument has to be a number\n");
      return -1;
    }
  }

  mode mode;
  if (strcmp(argv[3], "cp") == 0) {
    mode = MODE_CP;
  } else if (strcmp(argv[3], "mem") == 0) {
    mode = MODE_MEM;
  } else {
    fprintf(stderr, "The third argument has to be 'mem' or 'cp'\n");
    return -1;
  }

  int time_limit;
  {
    char* end;
    time_limit = strtol(argv[4], &end, 10);
    if (*end != 0) {
      fprintf(stderr, "The fourth argument has to be a number\n");
      return -1;
    }
  }

  int mem_limit;
  {
    char* end;
    mem_limit = strtol(argv[5], &end, 10);
    if (*end != 0) {
      fprintf(stderr, "The fifth argument has to be a number\n");
      return -1;
    }
  }

  file_array* arr = parse_file(argv[1]);
  pid_t* child_pids = calloc(sizeof(pid_t), arr->size);
  if (arr == NULL) {
    return -1;
  }
  for (size_t i = 0; i < arr->size; i++) {
    pid_t pid = fork();
    if (pid == 0) {
      struct rlimit limits;
      limits.rlim_max = time_limit;
      limits.rlim_cur = limits.rlim_max;
      if (setrlimit(RLIMIT_CPU, &limits) != 0) {
        perror("Couldn't set cpu limit");
        exit(-1);
      }
      limits.rlim_max = mem_limit * 1024 * 1024;
      limits.rlim_cur = limits.rlim_max;
      if (setrlimit(RLIMIT_AS, &limits) != 0) {
        perror("Couldn't set memory limit");
        exit(-1);
      }
      monitor(&arr->entries[i], duration, mode);
    } else {
      child_pids[i] = pid;
    }
  }

  for (size_t i = 0; i < arr->size; i++) {
    int status;
    waitpid(child_pids[i], &status, 0);
    printf("Process %d made %d copies\n", child_pids[i], WEXITSTATUS(status));
  }

  return 0;
}
