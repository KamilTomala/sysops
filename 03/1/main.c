#define _XOPEN_SOURCE 500
#include <dirent.h>
#include <ftw.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <wait.h>

void search(const char* base, const char* rel) {
  size_t fullname_length = strlen(base) + strlen(rel) + 1;
  char* fullname = calloc(fullname_length, 1);
  snprintf(fullname, fullname_length, "%s%s", base, rel);
  DIR* d = opendir(fullname);
  if (d == NULL) {
    return;
  }
  struct dirent* e;
  printf("Pid: %d, path: %s\n", getpid(), rel);
  if (fork() == 0) {
    chdir(fullname);
    execlp("ls", "ls", "-l", NULL);
  } else {
    wait(NULL);
  }
  free(fullname);
  while ((e = readdir(d)) != NULL) {
    if (strcmp(e->d_name, ".") == 0 || strcmp(e->d_name, "..") == 0) {
      continue;
    }
    // one character for path separator, one character for zero terminator
    size_t newrel_length = strlen(rel) + strlen(e->d_name) + 2;
    char* newrel = calloc(newrel_length, 1);
    snprintf(newrel, newrel_length, "%s/%s", rel, e->d_name);

    size_t newfull_length = newrel_length + strlen(base) + 2;
    char* newfull = calloc(newfull_length, 1);
    snprintf(newfull, newfull_length, "%s/%s", base, newrel);

    struct stat s;
    lstat(newfull, &s);
    if (S_ISDIR(s.st_mode)) {
      if (fork() == 0) {
        search(base, newrel);
        exit(0);
      } else {
        wait(NULL);
      }
    }
    free(newrel);
    free(newfull);
  }
  closedir(d);
}

int main(int argc, char** argv) {
  if (argc < 2) {
    fprintf(stderr, "Usage: main <directory>\n");
    return -1;
  }
  char dir_abs[512];
  realpath(argv[1], dir_abs);
  search(dir_abs, "");
  return 0;
}
