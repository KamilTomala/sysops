#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

struct cart;
typedef struct cart cart;

typedef struct {
  pthread_t thread;
  int id;
  cart* cart;
  bool is_chosen;
} passenger;

typedef enum {
  CART_UNLOADING,
  CART_LOADING,
  CART_READY,
  CART_GOING
} cart_state;

struct cart {
  pthread_mutex_t mut;
  pthread_cond_t cond;
  pthread_t thread;
  int id;
  int rounds_remaining;
  cart_state state;
  size_t capacity;
  size_t seats_taken;
  passenger* passengers[];
};

typedef struct {
  pthread_mutex_t mut;
  size_t capacity;
  size_t active_carts;
  pthread_cond_t station_cond;
  cart* station;
  size_t enter_index;
  size_t leave_index;
  cart** track;
} park;

typedef struct {
  cart* self;
  park* park;
} cart_args;

void print_msg(const char* msg, int id) {
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  long long m = t.tv_sec * 1000 + t.tv_nsec / 1000 / 1000;
  printf("[%010lld] @%02d %s\n", m, id, msg);
}

void lock(pthread_mutex_t* mut, const char* descr) {
  int attempts = 0;
  while (pthread_mutex_lock(mut)) {
    perror("Couldn't lock mutex");
    attempts++;
    if (attempts > 3) {
      fprintf(stderr, "Mutex failure at %s\n", descr);
      exit(EXIT_FAILURE);
    }
  }
}

void cart_enter(park* p, cart* c) {
  lock(&p->mut, "cart_enter");
  p->track[p->enter_index] = c;
  p->enter_index++;
  if (p->enter_index >= p->capacity) {
    p->enter_index -= p->capacity;
  }
  pthread_mutex_unlock(&p->mut);
}

void* cart_thread(void* va) {
  cart_args* args = va;
  park* park = args->park;
  cart* self = args->self;

  while (true) {
    // wait until i'm first in the queue to leave the track
    lock(&park->mut, "wait_to_leave_track");
    while (park->track[park->leave_index] != self) {
      pthread_cond_wait(&park->station_cond, &park->mut);
    }
    pthread_mutex_unlock(&park->mut);

    // wait until station is empty
    lock(&park->mut, "wait_station_empty");
    while (park->station != NULL) {
      pthread_cond_wait(&park->station_cond, &park->mut);
    }
    pthread_mutex_unlock(&park->mut);

    // move into station
    lock(&park->mut, "cart_arrive");
    park->station = self;
    park->leave_index++;
    while (park->leave_index >= park->capacity) {
      park->leave_index -= park->capacity;
    }
    pthread_cond_broadcast(&park->station_cond);
    pthread_mutex_unlock(&park->mut);

    print_msg("Cart arrived at station", self->id);

    // start unloading
    lock(&self->mut, "start_unloading");
    self->state = CART_UNLOADING;
    pthread_cond_broadcast(&self->cond);
    pthread_mutex_unlock(&self->mut);

    // wait until unloading is done
    lock(&self->mut, "wait_unload");
    while (self->seats_taken != 0) {
      pthread_cond_wait(&self->cond, &self->mut);
    }
    pthread_mutex_unlock(&self->mut);

    if (self->rounds_remaining <= 0) {
      break;
    }

    // start loading
    lock(&self->mut, "start_loading");
    self->state = CART_LOADING;
    pthread_mutex_unlock(&self->mut);

    lock(&park->mut, "notify_passengers");
    pthread_cond_broadcast(&park->station_cond);
    pthread_mutex_unlock(&park->mut);

    // wait until loading is done
    lock(&self->mut, "wait_load");
    while (self->seats_taken != self->capacity) {
      pthread_cond_wait(&self->cond, &self->mut);
    }
    pthread_mutex_unlock(&self->mut);

    print_msg("You cannot leave anymore", self->id);

    // nominate the start button pusher
    lock(&self->mut, "chose_push");
    int pusher = rand() % self->capacity;
    self->passengers[pusher]->is_chosen = true;
    self->state = CART_READY;
    pthread_cond_broadcast(&self->cond);
    pthread_mutex_unlock(&self->mut);

    // wait until start button is pushed
    lock(&self->mut, "wait_start");
    while (self->state != CART_GOING) {
      pthread_cond_wait(&self->cond, &self->mut);
    }
    pthread_mutex_unlock(&self->mut);

    print_msg("Enjoy Mr. Bones' Wild Ride!", self->id);

    // depart
    cart_enter(park, self);
    lock(&park->mut, "leave_station");
    park->station = NULL;
    pthread_cond_broadcast(&park->station_cond);
    pthread_mutex_unlock(&park->mut);

    // have fun
    int duration = rand() % 10 + 1;
    usleep(duration * 1000);
    // rinse & repeat
    self->rounds_remaining--;

    print_msg("The ride is over. Or, ... is it?", self->id);
  }

  print_msg("This cart is but another spooky corpse", self->id);
  lock(&park->mut, "cart_deactivate");
  park->active_carts--;
  if (park->station == self) {
    park->station = NULL;
    pthread_cond_broadcast(&park->station_cond);
  }
  if (park->active_carts == 0) {
    pthread_cond_broadcast(&park->station_cond);
  }
  pthread_mutex_unlock(&park->mut);
  return NULL;
}

typedef struct {
  park* park;
  passenger* self;
} passenger_args;

void* passenger_thread(void* va) {
  passenger_args* args = va;
  park* park = args->park;
  passenger* self = args->self;

  print_msg("I'm excited for MR. BONES WILD RIDE", self->id);

  while (true) {
    // wait until cart there is a cart at station, and self can get on
    lock(&park->mut, "wait_for_cart");
    while (park->active_carts != 0 &&
           (park->station == NULL || park->station->state != CART_LOADING ||
            park->station->seats_taken >= park->station->capacity)) {
      pthread_cond_wait(&park->station_cond, &park->mut);
    }

    if (park->active_carts == 0) {
      pthread_mutex_unlock(&park->mut);
      break;
    }

    self->cart = park->station;
    lock(&self->cart->mut, "get_on_cart");
    self->cart->passengers[self->cart->seats_taken] = self;
    self->cart->seats_taken++;
    char buff[64] = {0};
    snprintf(buff, sizeof(buff),
             "I can't wait for the ride to start (%zd / %zd)",
             self->cart->seats_taken, self->cart->capacity);
    print_msg(buff, self->id);
    if (self->cart->seats_taken == self->cart->capacity) {
      pthread_cond_broadcast(&self->cart->cond);
    }
    pthread_mutex_unlock(&park->mut);
    pthread_mutex_unlock(&self->cart->mut);

    // wait until it's loaded
    lock(&self->cart->mut, "wait_for_load");
    while (self->cart->seats_taken != self->cart->capacity) {
      pthread_cond_wait(&self->cart->cond, &self->cart->mut);
    }
    pthread_mutex_unlock(&self->cart->mut);

    lock(&self->cart->mut, "wait_for_choice");
    while (self->cart->state == CART_LOADING) {
      pthread_cond_wait(&self->cart->cond, &self->cart->mut);
    }
    pthread_mutex_unlock(&self->cart->mut);

    // press the button if i'm worthy
    if (self->is_chosen) {
      print_msg("I get to press the button! Lucky!", self->id);
      lock(&self->cart->mut, "press_button");
      self->cart->state = CART_GOING;
      pthread_cond_broadcast(&self->cart->cond);
      self->is_chosen = false;
      pthread_mutex_unlock(&self->cart->mut);
    }
    // wait until the ride ends
    lock(&self->cart->mut, "wait_ride_end");
    while (self->cart->state != CART_UNLOADING && self->cart->state) {
      pthread_cond_wait(&self->cart->cond, &self->cart->mut);
    }
    self->cart->seats_taken--;
    snprintf(buff, sizeof(buff),
             "I want to get off MR BONES WILD RIDE (%zd / %zd)",
             self->cart->seats_taken, self->cart->capacity);
    print_msg(buff, self->id);
    if (self->cart->seats_taken == 0) {
      pthread_cond_broadcast(&self->cart->cond);
    }
    pthread_mutex_unlock(&self->cart->mut);
    self->cart = NULL;
  }

  print_msg("Are we stuck here?", self->id);

  return NULL;
}

int main(int argc, char** argv) {
  if (argc != 5) {
    printf("Usage: %s <cart count> <passenger count> <cart capacity> <round "
           "count>\n",
           argv[0]);
    exit(EXIT_FAILURE);
  }
  char* arg_end;
  size_t cart_count = strtoul(argv[1], &arg_end, 10);
  if (*arg_end != 0) {
    printf("'%s' is not a number", argv[1]);
  }
  size_t passenger_count = strtoul(argv[2], &arg_end, 10);
  if (*arg_end != 0) {
    printf("'%s' is not a number", argv[2]);
  }
  size_t cart_capacity = strtoul(argv[3], &arg_end, 10);
  if (*arg_end != 0) {
    printf("'%s' is not a number", argv[3]);
  }
  int round_count = strtoul(argv[4], &arg_end, 10);
  if (*arg_end != 0) {
    printf("'%s' is not a number", argv[4]);
  }

  park* park = malloc(sizeof(*park));
  pthread_mutex_init(&park->mut, NULL);
  pthread_cond_init(&park->station_cond, NULL);
  park->capacity = cart_count;
  park->active_carts = cart_count;
  park->enter_index = park->leave_index = 0;
  park->station = NULL;
  park->track = calloc(sizeof(cart*), cart_count);

  pthread_t* threads = calloc(cart_count + passenger_count, sizeof(threads[0]));
  for (size_t i = 0; i < cart_count; i++) {
    cart* c = malloc(sizeof(*c) + cart_capacity * sizeof(c->passengers[0]));
    cart_enter(park, c);

    c->id = i;
    c->rounds_remaining = round_count;
    c->state = CART_UNLOADING;
    c->capacity = cart_capacity;
    c->seats_taken = 0;
    pthread_mutex_init(&c->mut, NULL);
    pthread_cond_init(&c->cond, NULL);

    cart_args* args = malloc(sizeof(*args));
    args->self = c;
    args->park = park;
    pthread_create(&c->thread, NULL, cart_thread, args);

    threads[i] = c->thread;
  }
  for (size_t i = 0; i < passenger_count; i++) {
    passenger* p = malloc(sizeof(*p));

    p->id = i;
    p->cart = NULL;
    p->is_chosen = false;

    passenger_args* args = malloc(sizeof(*args));
    args->self = p;
    args->park = park;
    pthread_create(&p->thread, NULL, passenger_thread, args);

    threads[cart_count + i] = p->thread;
  }

  for (size_t i = 0; i < cart_count + passenger_count; i++) {
    pthread_join(threads[i], NULL);
  }

  return 0;
}
