#include <dirent.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

typedef enum mode { MODE_GT, MODE_LT, MODE_EQ } mode;

typedef struct dirent dirent;

void search(const char* dir, mode m, long ref) {
  DIR* d = opendir(dir);
  if (d == NULL) {
    fprintf(stderr, "Directory %s was null\n", dir);
    return;
  }
  dirent* e;
  while ((e = readdir(d)) != NULL) {
    if (strcmp(e->d_name, ".") == 0 || strcmp(e->d_name, "..") == 0) {
      continue;
    }
    // one character for path separator, one character for zero terminator
    size_t fullname_length = strlen(dir) + strlen(e->d_name) + 2;
    char* fullname = calloc(fullname_length, 1);
    snprintf(fullname, fullname_length, "%s/%s", dir, e->d_name);
    struct stat s;
    lstat(fullname, &s);
    bool pred;
    switch (m) {
    case MODE_GT:
      pred = s.st_size > ref;
      break;
    case MODE_LT:
      pred = s.st_size < ref;
      break;
    case MODE_EQ:
      pred = s.st_size == ref;
      break;
    }
    if (pred) {
      char* type = "???";
      if (S_ISREG(s.st_mode)) {
        type = "file";
      } else if (S_ISDIR(s.st_mode)) {
        type = "dir";
      } else if (S_ISCHR(s.st_mode)) {
        type = "char dev";
      } else if (S_ISBLK(s.st_mode)) {
        type = "block dev";
      } else if (S_ISFIFO(s.st_mode)) {
        type = "fifo";
      } else if (S_ISLNK(s.st_mode)) {
        type = "slink";
      } else if (S_ISSOCK(s.st_mode)) {
        type = "sock";
      }
      struct tm* tm_created = localtime(&s.st_mtime);
      char time_created[16];
      strftime(time_created, 64, "%d.%m.%Y", tm_created);
      struct tm* tm_modified = localtime(&s.st_ctime);
      char time_modified[16];
      strftime(time_modified, 64, "%d.%m.%Y", tm_modified);
      printf("%s %s %ld %s %s\n", fullname, type, s.st_size, time_created,
             time_modified);
    }
    if (S_ISDIR(s.st_mode)) {
      search(fullname, m, ref);
    }
    free(fullname);
  }
  closedir(d);
}

int main(int argc, char** argv) {
  if (argc < 4) {
    fprintf(stderr, "Usage: main <directory> <operator> <value>\n");
    return -1;
  }
  mode m;
  if (strcmp(argv[2], "=") == 0) {
    m = MODE_EQ;
  } else if (strcmp(argv[2], ">") == 0) {
    m = MODE_GT;
  } else if (strcmp(argv[2], "<") == 0) {
    m = MODE_LT;
  } else {
    fprintf(stderr, "Operator needs to be one of: '=', '>' or '<'\n");
    return -1;
  }
  char* ref_end;
  long value = strtol(argv[3], &ref_end, 10);
  if (*ref_end != 0) {
    fprintf(stderr, "Value has to be an integer.\n");
    return -1;
  }
  char dir_abs[512];
  realpath(argv[1], dir_abs);
  search(dir_abs, m, value);
  return 0;
}
