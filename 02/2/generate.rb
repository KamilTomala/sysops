#!/usr/bin/ruby
require 'fileutils'

Depth = 5
Files = 20
Links = 5
FileSizeMin = 0
FileSizeMax = 1024
Dirs = 3

def gen path, depth
  Files.times do |i|
    File.open("#{path}/f#{i}", "w") do |f|
      size = Random.rand(FileSizeMin .. FileSizeMax)
      f.puts "A" * size
    end
  end
  Links.times do |i|
    link_depth = Random.rand(0 .. (Depth - depth))
    link_name = case Random.rand(2)
                when 0
                  "f#{Random.rand(0 ... Files)}"
                when 1
                  "d#{Random.rand(0 ... Dirs)}"
                end
    link_path = (1 .. link_depth)
                  .map {"d#{Random.rand(0 ... Dirs)}" }
                  .join("/") + "/#{link_name}"
    FileUtils.ln_s link_path, "#{path}/l#{i}"
  end
  if depth < Depth
    Dirs.times do |i|
      p = "#{path}/d#{i}"
      FileUtils.mkdir p
      gen p, (depth + 1)
    end
  end
end

gen ARGV[0], 0
