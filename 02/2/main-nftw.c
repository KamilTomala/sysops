#define _XOPEN_SOURCE 500
#include <dirent.h>
#include <ftw.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

typedef enum mode { MODE_GT, MODE_LT, MODE_EQ } mode;

typedef struct dirent dirent;

mode m;
long ref;

int print_entry(const char* path, const struct stat* sb, int typeflag,
                struct FTW* ftwbuf) {
  bool pred;
  switch (m) {
  case MODE_GT:
    pred = sb->st_size > ref;
    break;
  case MODE_LT:
    pred = sb->st_size < ref;
    break;
  case MODE_EQ:
    pred = sb->st_size == ref;
    break;
  }
  if (pred) {
    char* type = "???";
    if (S_ISREG(sb->st_mode)) {
      type = "file";
    } else if (S_ISDIR(sb->st_mode)) {
      type = "dir";
    } else if (S_ISCHR(sb->st_mode)) {
      type = "char dev";
    } else if (S_ISBLK(sb->st_mode)) {
      type = "block dev";
    } else if (S_ISFIFO(sb->st_mode)) {
      type = "fifo";
    } else if (S_ISLNK(sb->st_mode)) {
      type = "slink";
    } else if (S_ISSOCK(sb->st_mode)) {
      type = "sock";
    }
    struct tm* tm_created = localtime(&sb->st_mtime);
    char time_created[16];
    strftime(time_created, 64, "%d.%m.%Y", tm_created);
    struct tm* tm_modified = localtime(&sb->st_ctime);
    char time_modified[16];
    strftime(time_modified, 64, "%d.%m.%Y", tm_modified);
    printf("%s %s %ld %s %s\n", path, type, sb->st_size, time_created,
           time_modified);
  }
  return 0;
}

int main(int argc, char** argv) {
  if (argc < 4) {
    fprintf(stderr, "Usage: main <directory> <operator> <value>\n");
    return -1;
  }
  if (strcmp(argv[2], "=") == 0) {
    m = MODE_EQ;
  } else if (strcmp(argv[2], ">") == 0) {
    m = MODE_GT;
  } else if (strcmp(argv[2], "<") == 0) {
    m = MODE_LT;
  } else {
    fprintf(stderr, "Operator needs to be one of: '=', '>' or '<'\n");
    return -1;
  }
  char* ref_end;
  ref = strtol(argv[3], &ref_end, 10);
  if (*ref_end != 0) {
    fprintf(stderr, "Value has to be an integer.\n");
    return -1;
  }
  char dir_abs[512];
  realpath(argv[1], dir_abs);
  nftw(dir_abs, print_entry, 2, FTW_PHYS);
  return 0;
}
