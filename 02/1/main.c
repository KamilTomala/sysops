#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/times.h>

typedef enum mode { SYS, LIB } mode;

typedef union file {
  FILE* lib;
  int sys;
} file;

int generate(char* filename, size_t size, size_t count, mode m) {
  file f;
  switch (m) {
  case SYS:
    f.sys = open(filename, O_WRONLY);
    break;
  case LIB:
    f.lib = fopen(filename, "w");
    break;
  }
  srand(time(NULL));
  unsigned char* buffer = malloc(size);
  for (size_t i = 0; i < count; i++) {
    for (size_t j = 0; j < size; j++) {
      buffer[j] = rand() & 0xff;
    }
    switch (m) {
    case SYS:
      write(f.sys, buffer, size);
      break;
    case LIB:
      fwrite(buffer, size, 1, f.lib);
      break;
    }
  }
  switch (m) {
  case SYS:
    close(f.sys);
    break;
  case LIB:
    fclose(f.lib);
    break;
  }
  return 0;
}

int copy(char* src, char* dest, size_t size, size_t count, mode m) {
  file s, d;
  switch (m) {
  case SYS:
    s.sys = open(src, O_RDONLY);
    d.sys = open(dest, O_WRONLY);
    break;
  case LIB:
    s.lib = fopen(src, "r");
    d.lib = fopen(dest, "w");
    break;
  }

  unsigned char* buffer = malloc(size);

  for (size_t i = 0; i < count; i++) {
    switch (m) {
    case SYS:
      read(s.sys, buffer, size);
      write(d.sys, buffer, size);
      break;
    case LIB:
      fread(buffer, size, 1, s.lib);
      fwrite(buffer, size, 1, d.lib);
      break;
    }
  }

  free(buffer);
  switch (m) {
  case SYS:
    close(s.sys);
    close(d.sys);
    break;
  case LIB:
    fclose(s.lib);
    fclose(d.lib);
    break;
  }
  return 0;
}

int sort(char* filename, size_t size, size_t count, mode m) {
  file f;
  switch (m) {
  case SYS:
    f.sys = open(filename, O_RDWR);
    break;
  case LIB:
    f.lib = fopen(filename, "r+b");
    break;
  }

  unsigned char* current_buffer = malloc(size);
  unsigned char* swap_buffer = malloc(size);

  for (size_t insert_index = 0; insert_index < count; insert_index++) {
    size_t swap_index = insert_index;
    unsigned char min;
    switch (m) {
    case SYS:
      lseek(f.sys, swap_index * size, SEEK_SET);
      read(f.sys, &min, 1);
      break;
    case LIB:
      fseek(f.lib, swap_index * size, SEEK_SET);
      min = fgetc(f.lib);
      break;
    }
    for (size_t i = swap_index + 1; i < count; i++) {
      unsigned char c;
      switch (m) {
      case SYS:
        lseek(f.sys, i * size, SEEK_SET);
        read(f.sys, &c, 1);
        break;
      case LIB:
        fseek(f.lib, i * size, SEEK_SET);
        c = fgetc(f.lib);
        break;
      }
      if (c < min) {
        min = c;
        swap_index = i;
      }
    }
    if (swap_index != insert_index) {
      switch (m) {
      case SYS:
        lseek(f.sys, insert_index * size, SEEK_SET);
        read(f.sys, current_buffer, size);
        lseek(f.sys, swap_index * size, SEEK_SET);
        read(f.sys, swap_buffer, size);

        lseek(f.sys, insert_index * size, SEEK_SET);
        write(f.sys, swap_buffer, size);
        lseek(f.sys, swap_index * size, SEEK_SET);
        write(f.sys, current_buffer, size);
        break;
      case LIB:
        fseek(f.lib, insert_index * size, SEEK_SET);
        fread(current_buffer, size, 1, f.lib);
        fseek(f.lib, swap_index * size, SEEK_SET);
        fread(swap_buffer, size, 1, f.lib);

        fseek(f.lib, insert_index * size, SEEK_SET);
        fwrite(swap_buffer, size, 1, f.lib);
        fseek(f.lib, swap_index * size, SEEK_SET);
        fwrite(current_buffer, size, 1, f.lib);
        break;
      }
    }
  }

  free(current_buffer);
  free(swap_buffer);
  switch (m) {
  case SYS:
    close(f.sys);
    break;
  case LIB:
    fclose(f.lib);
    break;
  }
  return 0;
}

void printUsage() {
  fprintf(stderr, "Available commands:\n");
  fprintf(stderr, " generate <file> <record_size> <record_count> <mode>\n");
  fprintf(stderr,
          " copy <source> <destination> <record_size> <record_count> <mode>\n");
  fprintf(stderr, " sort <file> <record_size> <record_count> <mode>\n");
}

mode parse_mode(char* arg) {
  if (strcmp(arg, "sys") == 0) {
    return SYS;
  } else if (strcmp(arg, "lib") == 0) {
    return LIB;
  } else {
    fprintf(stderr, "Wrong mode: %s\n", arg);
    exit(-1);
  }
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printUsage();
    return 0;
  }
  struct tms tms_before;
  times(&tms_before);
  if (strcmp(argv[1], "generate") == 0) {
    if (argc < 6) {
      fprintf(stderr,
              "Usage: generate <file> <record_size> <record_count> <mode>\n");
      return -1;
    }
    generate(argv[2], atoi(argv[3]), atoi(argv[4]), parse_mode(argv[5]));
  } else if (strcmp(argv[1], "copy") == 0) {
    if (argc < 7) {
      fprintf(stderr, "Usage: copy <source> <destination> <record_size> "
                      "<record_count> <mode>\n");
      return -1;
    }
    copy(argv[2], argv[3], atoi(argv[4]), atoi(argv[5]), parse_mode(argv[6]));
  } else if (strcmp(argv[1], "sort") == 0) {
    if (argc < 6) {
      fprintf(stderr,
              "Usage: sort <file> <record_size> <record_count> <mode>\n");
      return -1;
    }
    sort(argv[2], atoi(argv[3]), atoi(argv[4]), parse_mode(argv[5]));
  } else {
    printUsage();
    return 0;
  }
  struct tms tms_after;
  times(&tms_after);
  clock_t user = tms_after.tms_utime - tms_before.tms_utime;
  clock_t sys = tms_after.tms_stime - tms_before.tms_stime;
  unsigned tps = sysconf(_SC_CLK_TCK);
  printf("user %10ldms system %10ldms\n", user * 1000 / tps, sys * 1000 / tps);
  return 0;
}
