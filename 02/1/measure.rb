#!/usr/bin/ruby

record_sizes = [1, 4, 512, 1024, 4096, 8192]

record_counts = [1024, 16384]

def extract_out str
  res = /user\s*(\d+ms) system\s*(\d+ms)/.match(str)
  [res[1], res[2]]
end

puts "%6s %6s %5s %10s %10s %10s %10s" %
     ["count", "size", "time", "copy sys", "copy lib", "sort sys", "sort lib"]

record_sizes.product(record_counts).each do |size, count|
  `out/main generate temp #{size} #{count} lib`

  copy_sys = extract_out(`out/main copy temp temps #{size} #{count} sys`)
  copy_lib = extract_out(`out/main copy temp templ #{size} #{count} lib`)
  sort_sys = extract_out(`out/main sort temps #{size} #{count} sys`)
  sort_lib = extract_out(`out/main sort templ #{size} #{count} lib`)

  puts "%6d %6d %5s %10s %10s %10s %10s" %
       [count, size, "user", copy_sys[0], copy_lib[0], sort_sys[0], sort_lib[0]]
  puts "%6s %6s %5s %10s %10s %10s %10s" %
       ["", "", "sys", copy_sys[1], copy_lib[1], sort_sys[1], sort_lib[1]]
end
