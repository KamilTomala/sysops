#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <time.h>
#include <unistd.h>

#include "common.h"

production_line* open_production_line() {
  key_t key = ftok(getenv("HOME"), PRODUCTION_LINE_ID);
  int id = shmget(key, sizeof(production_line), 0);
  if (id < 0) {
    perror("Couldn't open shared memory");
    exit(EXIT_FAILURE);
  }
  production_line* line = shmat(id, NULL, 0);
  size_t capacity = line->capacity;
  shmdt(line);
  id = shmget(key, sizeof(production_line) + capacity + sizeof(package), 0);
  line = shmat(id, NULL, 0);

  return line;
}

int open_semaphores() {
  key_t key = ftok(getenv("HOME"), SEMAPHORE_SET_ID);
  int id = semget(key, SEM_COUNT, 0);
  if (id < 0) {
    perror("Couldn't open semaphores");
    exit(EXIT_FAILURE);
  }
  return id;
}

void print_time() {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  long long int t = now.tv_sec * 1000 * 1000 + now.tv_nsec / 1000;
  printf("[%011lld] ", t);
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("Usage: %s <mass> [cycle_count]\n", argv[0]);
    return EXIT_SUCCESS;
  }

  char* end;
  int mass = strtol(argv[1], &end, 10);
  if (*end != 0) {
    printf("'%s' is not a number\n", argv[1]);
    exit(EXIT_FAILURE);
  }
  int count;
  bool limit = false;
  if (argc >= 3) {
    limit = true;
    count = strtol(argv[2], &end, 10);
    if (*end != 0) {
      printf("'%s' is not a number\n", argv[2]);
      exit(EXIT_FAILURE);
    }
  }

  production_line* line = open_production_line();
  int semaphores = open_semaphores();

  while (!limit || count > 0) {
    if (line->max_mass < mass) {
      line->max_mass = mass;
    }
    usleep(100 * 1000);
    struct sembuf op[] = {{SEM_OPEN, 0, 0},
                          {SEM_WRITE, -1, 0},
                          {SEM_MASS, -mass, 0},
                          {SEM_FREE, -1, 0}};
    print_time();
    printf("Waiting to load a package\n");
    if (semop(semaphores, op, ARR_LENGTH(op)) == 0) {
      struct timespec now;
      clock_gettime(CLOCK_MONOTONIC, &now);
      line->packages[line->write_index] =
          (package){.mass = mass, .pid = getpid(), .time = now};
      line->write_index = (line->write_index + 1) % line->capacity;
      print_time();
      printf("Produced a package\n");
      if (limit) {
        count--;
      }

      if (semctl(semaphores, SEM_MASS, GETVAL) <= line->max_mass) {
        struct sembuf op[] = {{SEM_OPEN, +1, 0}};
        semop(semaphores, op, ARR_LENGTH(op));
      }

      struct sembuf op[] = {{SEM_WRITE, +1, 0}, {SEM_AVAILABLE, +1, 0}};
      semop(semaphores, op, ARR_LENGTH(op));
    }
  }

  line->max_mass = -1;

  return EXIT_SUCCESS;
}
