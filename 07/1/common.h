#include <stdlib.h>

#define ARR_LENGTH(a) (sizeof(a) / sizeof((a)[0]))

const int PRODUCTION_LINE_ID = 8934108;
const int SEMAPHORE_SET_ID = 1400476186;

union semun {
  int val;
  struct semid_ds* buf;
  unsigned short int* array;
  struct seminfo* __buf;
};

typedef struct {
  int mass;
  pid_t pid;
  struct timespec time;
} package;

typedef struct {
  size_t capacity;
  int mass_cap;
  int max_mass;
  size_t write_index;
  size_t read_index;
  package packages[];
} production_line;

enum { SEM_FREE, SEM_AVAILABLE, SEM_MASS, SEM_OPEN, SEM_WRITE, SEM_COUNT };
