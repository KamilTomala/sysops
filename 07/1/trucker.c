#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <time.h>
#include <unistd.h>

#include "common.h"

production_line* create_production_line(size_t capacity, int* shm) {
  key_t key = ftok(getenv("HOME"), PRODUCTION_LINE_ID);
  *shm = shmget(key, sizeof(production_line) + capacity * sizeof(package),
                IPC_CREAT | IPC_EXCL | 0666);
  if (*shm < 0) {
    perror("Couldn't create shared memory");
    exit(EXIT_FAILURE);
  }
  production_line* line = shmat(*shm, NULL, 0);
  line->capacity = capacity;
  line->read_index = 0;
  line->write_index = 0;
  line->max_mass = -1;
  return line;
}

int create_semaphores(size_t capacity, int mass_capacity) {
  key_t key = ftok(getenv("HOME"), SEMAPHORE_SET_ID);
  int id = semget(key, SEM_COUNT, IPC_CREAT | IPC_EXCL | 0666);
  if (id < 0) {
    perror("Couldn't create semaphores");
    exit(EXIT_FAILURE);
  }
  unsigned short vals[SEM_COUNT];
  vals[SEM_FREE] = capacity;
  vals[SEM_AVAILABLE] = 0;
  vals[SEM_OPEN] = 0;
  vals[SEM_MASS] = mass_capacity;
  vals[SEM_WRITE] = 1;
  semctl(id, 0, SETALL, (union semun){.array = vals});
  return id;
}

volatile bool terminate = false;

void handle_int(int sig) {
  terminate = true;
}

void print_time() {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  long long int t = now.tv_sec * 1000 * 1000 + now.tv_nsec / 1000;
  printf("[%011lld] ", t);
}

int main(int argc, char** argv) {
  if (argc < 4) {
    printf("Usage: %s <truck_capacity> <line_capacity> <line_mass_capacity>\n",
           argv[0]);
    return EXIT_SUCCESS;
  }

  char* end;
  int truck_capacity = strtol(argv[1], &end, 10);
  if (*end != 0) {
    printf("'%s' is not a number\n", argv[1]);
    exit(EXIT_FAILURE);
  }
  int line_capacity = strtol(argv[2], &end, 10);
  if (*end != 0) {
    printf("'%s' is not a number\n", argv[2]);
    exit(EXIT_FAILURE);
  }
  int line_mass_capacity = strtol(argv[3], &end, 10);
  if (*end != 0) {
    printf("'%s' is not a number\n", argv[3]);
    exit(EXIT_FAILURE);
  }

  sigaction(SIGINT,
            &(const struct sigaction){.sa_handler = handle_int, .sa_flags = 0},
            NULL);

  int line_id;
  production_line* line = create_production_line(line_capacity, &line_id);
  int semaphores = create_semaphores(line_capacity, line_mass_capacity);
  line->mass_cap = line_mass_capacity;

  bool closing = false;
  bool closed = false;
  int packages_in_truck = 0;

  while (!closed) {
    print_time();
    printf("Empty truck arrived\n");
    while (packages_in_truck < truck_capacity) {
      if (!closing && terminate) {
        closing = true;
        struct sembuf op[] = {{SEM_OPEN, +2, 0}};
        semop(semaphores, op, ARR_LENGTH(op));
      }
      if (closing) {
        if (line->write_index == line->read_index) {
          closed = true;
          break;
        }
      }
      sleep(1);
      struct sembuf op[] = {{SEM_AVAILABLE, -1, 0}};
      if (semop(semaphores, op, ARR_LENGTH(op)) != 0) {
        perror("Couldn't wait on semaphore");
        continue;
      }

      print_time();
      printf("Packages on the line: ");
      if (line->write_index <= line->read_index) {
        for (size_t i = line->read_index; i < line->capacity; i++) {
          printf("%d ", line->packages[i].mass);
        }
        for (size_t i = 0; i < line->write_index; i++) {
          printf("%d ", line->packages[i].mass);
        }
      } else {
        for (size_t i = line->read_index; i < line->write_index; i++) {
          printf("%d ", line->packages[i].mass);
        }
      }
      printf("\n");

      package pkg = line->packages[line->read_index];
      line->read_index = (line->read_index + 1) % line->capacity;
      packages_in_truck++;
      struct timespec now;
      clock_gettime(CLOCK_MONOTONIC, &now);
      long elapsed = (now.tv_sec - pkg.time.tv_sec) * 1000 +
                     (now.tv_nsec - pkg.time.tv_nsec) / 1000000;
      print_time();
      printf("Loaded package {mass = %d, pid = %d, time waiting = %ld}\n",
             pkg.mass, pkg.pid, elapsed);
      print_time();
      printf("Packages in the truck: %d/%d\n", packages_in_truck,
             truck_capacity);
      {
        struct sembuf op[] = {{SEM_MASS, +pkg.mass, 0}, {SEM_FREE, +1, 0}};
        if (semop(semaphores, op, ARR_LENGTH(op)) != 0) {
          perror("Couldn't release mass");
          continue;
        }
      }
      if (semctl(semaphores, SEM_OPEN, GETVAL) == 1 &&
          semctl(semaphores, SEM_MASS, GETVAL) >= line->max_mass) {
        struct sembuf op[] = {{SEM_OPEN, -1, 0}};
        if (semop(semaphores, op, ARR_LENGTH(op)) != 0) {
          perror("Couldn't open the line");
          continue;
        }
      }
    }
    print_time();
    printf("Truck full, unloading");
    fflush(stdout);
    for (size_t i = 0; i < 8; i++) {
      usleep(500 * 1000);
      printf(".");
      fflush(stdout);
    }
    packages_in_truck = 0;
    printf("\n");
    print_time();
    printf("Done.\n");
  }

  semctl(semaphores, 0, IPC_RMID);
  shmdt(line);
  shmctl(line_id, IPC_RMID, NULL);

  return EXIT_SUCCESS;
}
