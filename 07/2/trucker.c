#include <fcntl.h>
#include <semaphore.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#include "common.h"

production_line* create_production_line(size_t capacity, int* shm) {
  *shm = shm_open(PRODUCTION_LINE_NAME, O_RDWR | O_CREAT | O_EXCL, 0666);
  if (*shm < 0) {
    perror("Couldn't create shared memory");
    exit(EXIT_FAILURE);
  }
  size_t size = sizeof(production_line) + capacity * sizeof(package);
  ftruncate(*shm, size);
  production_line* line =
      mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, *shm, 0);
  line->capacity = capacity;
  line->read_index = 0;
  line->write_index = 0;
  line->max_mass = -1;
  line->load = 0;
  return line;
}

semaphores create_semaphores(size_t capacity) {
  semaphores s = {};
  s.free = sem_open(SEM_FREE_NAME, O_RDWR | O_CREAT | O_EXCL, 0666, capacity);
  if (s.free == SEM_FAILED) {
    perror("Couldn't create the free semaphore");
    exit(EXIT_FAILURE);
  }
  s.available =
      sem_open(SEM_AVAILABLE_NAME, O_RDWR | O_CREAT | O_EXCL, 0666, 0);
  if (s.available == SEM_FAILED) {
    perror("Couldn't create the available semaphore");
    exit(EXIT_FAILURE);
  }
  s.write = sem_open(SEM_WRITE_NAME, O_RDWR | O_CREAT | O_EXCL, 0666, 1);
  if (s.write == SEM_FAILED) {
    perror("Couldn't create the write semaphore");
    exit(EXIT_FAILURE);
  }

  return s;
}

volatile bool terminate = false;

void handle_int(int sig) {
  terminate = true;
}

void print_time() {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  long long int t = now.tv_sec * 1000 * 1000 + now.tv_nsec / 1000;
  printf("[%011lld] ", t);
}

int main(int argc, char** argv) {
  if (argc < 4) {
    printf("Usage: %s <truck_capacity> <line_capacity> <line_mass_capacity>\n",
           argv[0]);
    return EXIT_SUCCESS;
  }

  char* end;
  int truck_capacity = strtol(argv[1], &end, 10);
  if (*end != 0) {
    printf("'%s' is not a number\n", argv[1]);
    exit(EXIT_FAILURE);
  }
  int line_capacity = strtol(argv[2], &end, 10);
  if (*end != 0) {
    printf("'%s' is not a number\n", argv[2]);
    exit(EXIT_FAILURE);
  }
  int line_mass_capacity = strtol(argv[3], &end, 10);
  if (*end != 0) {
    printf("'%s' is not a number\n", argv[3]);
    exit(EXIT_FAILURE);
  }

  sigaction(SIGINT,
            &(const struct sigaction){.sa_handler = handle_int, .sa_flags = 0},
            NULL);

  int line_id;
  production_line* line = create_production_line(line_capacity, &line_id);
  semaphores sem = create_semaphores(line_capacity);
  line->mass_cap = line_mass_capacity;

  bool closed = false;
  int packages_in_truck = 0;

  while (!closed) {
    print_time();
    printf("Empty truck arrived\n");
    while (packages_in_truck < truck_capacity) {
      if (!line->closing && terminate) {
        line->closing = true;
      }
      if (line->closing) {
        if (line->write_index == line->read_index) {
          closed = true;
          break;
        }
      }
      sleep(1);

      if (sem_wait(sem.available) != 0) {
        perror("Couldn't wait on semaphore");
        continue;
      }

      print_time();
      printf("Packages on the line: ");
      if (line->write_index <= line->read_index) {
        for (size_t i = line->read_index; i < line->capacity; i++) {
          printf("%d ", line->packages[i].mass);
        }
        for (size_t i = 0; i < line->write_index; i++) {
          printf("%d ", line->packages[i].mass);
        }
      } else {
        for (size_t i = line->read_index; i < line->write_index; i++) {
          printf("%d ", line->packages[i].mass);
        }
      }
      printf("\n");

      package pkg = line->packages[line->read_index];
      line->read_index = (line->read_index + 1) % line->capacity;
      packages_in_truck++;
      struct timespec now;
      clock_gettime(CLOCK_MONOTONIC, &now);
      long elapsed = (now.tv_sec - pkg.time.tv_sec) * 1000 +
                     (now.tv_nsec - pkg.time.tv_nsec) / 1000000;
      print_time();
      printf("Loaded package {mass = %d, pid = %d, time waiting = %ld}\n",
             pkg.mass, pkg.pid, elapsed);
      print_time();
      printf("Packages in the truck: %d/%d\n", packages_in_truck,
             truck_capacity);
      line->load -= pkg.mass;
      if (sem_post(sem.free) != 0) {
        perror("Couldn't release mass");
        continue;
      }
    }
    print_time();
    printf("Truck full, unloading");
    fflush(stdout);
    for (size_t i = 0; i < 8; i++) {
      usleep(500 * 1000);
      printf(".");
      fflush(stdout);
    }
    packages_in_truck = 0;
    printf("\n");
    print_time();
    printf("Done.\n");
  }

  sem_close(sem.free);
  sem_close(sem.available);
  sem_close(sem.write);
  sem_unlink(SEM_FREE_NAME);
  sem_unlink(SEM_AVAILABLE_NAME);
  sem_unlink(SEM_WRITE_NAME);

  munmap(line, sizeof(production_line) + line->capacity * sizeof(package));
  shm_unlink(PRODUCTION_LINE_NAME);

  return EXIT_SUCCESS;
}
