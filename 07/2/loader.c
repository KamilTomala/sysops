#include <fcntl.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <time.h>
#include <unistd.h>

#include "common.h"

production_line* open_production_line() {
  int id = shm_open(PRODUCTION_LINE_NAME, O_RDWR, 0666);
  if (id < 0) {
    perror("Couldn't open shared memory");
    exit(EXIT_FAILURE);
  }
  production_line* line = mmap(NULL, sizeof(production_line),
                               PROT_READ | PROT_WRITE, MAP_SHARED, id, 0);
  size_t cap = line->capacity;
  munmap(line, sizeof(production_line));
  line = mmap(NULL, sizeof(production_line) + cap * sizeof(package),
              PROT_READ | PROT_WRITE, MAP_SHARED, id, 0);
  return line;
}

semaphores open_semaphores() {
  semaphores s = {};
  s.free = sem_open(SEM_FREE_NAME, O_RDWR);
  if (s.free == SEM_FAILED) {
    perror("Couldn't open the free semaphore");
    exit(EXIT_FAILURE);
  }
  s.available = sem_open(SEM_AVAILABLE_NAME, O_RDWR);
  if (s.available == SEM_FAILED) {
    perror("Couldn't open the available semaphore");
    exit(EXIT_FAILURE);
  }
  s.write = sem_open(SEM_WRITE_NAME, O_RDWR);
  if (s.write == SEM_FAILED) {
    perror("Couldn't open the write semaphore");
    exit(EXIT_FAILURE);
  }

  return s;
}

void print_time() {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  long long int t = now.tv_sec * 1000 * 1000 + now.tv_nsec / 1000;
  printf("[%011lld] ", t);
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("Usage: %s <mass> [cycle_count]\n", argv[0]);
    return EXIT_SUCCESS;
  }

  char* end;
  int mass = strtol(argv[1], &end, 10);
  if (*end != 0) {
    printf("'%s' is not a number\n", argv[1]);
    exit(EXIT_FAILURE);
  }
  int count;
  bool limit = false;
  if (argc >= 3) {
    limit = true;
    count = strtol(argv[2], &end, 10);
    if (*end != 0) {
      printf("'%s' is not a number\n", argv[2]);
      exit(EXIT_FAILURE);
    }
  }

  production_line* line = open_production_line();
  semaphores sem = open_semaphores();

  while ((!limit || count > 0) && !line->closing) {
    if (line->max_mass < mass) {
      line->max_mass = mass;
    }
    usleep(100 * 1000);
    print_time();
    printf("Waiting for write\n");
    if (sem_wait(sem.write) == 0) {
      if (!line->closing && sem_wait(sem.free) == 0) {
        if (line->load + mass <= line->mass_cap &&
            line->load + line->max_mass <= line->mass_cap) {
          struct timespec now;
          clock_gettime(CLOCK_MONOTONIC, &now);
          line->packages[line->write_index] =
              (package){.mass = mass, .pid = getpid(), .time = now};
          line->write_index = (line->write_index + 1) % line->capacity;
          line->load += mass;
          print_time();
          printf("Produced a package\n");
          if (limit) {
            count--;
          }
        }
      }
      sem_post(sem.write);
      sem_post(sem.available);
    }
  }

  line->max_mass = -1;
  munmap(line, sizeof(production_line) + line->capacity * sizeof(package));
  sem_close(sem.free);
  sem_close(sem.available);
  sem_close(sem.write);

  return EXIT_SUCCESS;
}
