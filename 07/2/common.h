#include <semaphore.h>
#include <stdbool.h>
#include <stdlib.h>

#define ARR_LENGTH(a) (sizeof(a) / sizeof((a)[0]))

const char* PRODUCTION_LINE_NAME = "/296701-prod-line";
const int SEMAPHORE_SET_ID = 1400476186;

typedef struct {
  int mass;
  pid_t pid;
  struct timespec time;
} package;

typedef struct {
  size_t capacity;
  int mass_cap;
  int load;
  int max_mass;
  bool closing;
  size_t write_index;
  size_t read_index;
  package packages[];
} production_line;

const char* SEM_FREE_NAME = "/296701-sem-free";
const char* SEM_AVAILABLE_NAME = "/296701-sem-available";
const char* SEM_WRITE_NAME = "/296701-sem-write";

typedef struct {
  sem_t* free;
  sem_t* available;
  sem_t* write;
} semaphores;
