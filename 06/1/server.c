#include <ctype.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <time.h>

#include "msg.h"

int open_queue() {
  const char* home = getenv("HOME");
  key_t key = ftok(home, '6');
  int queue = msgget(key, IPC_CREAT | IPC_EXCL | 0622);
  if (queue < 0) {
    perror("Couldn't create server queue");
    exit(0);
  }
  return queue;
}

typedef struct {
  int queue;
  bool active;
  long friends[64];
} client;

typedef struct {
  size_t allocated;
  client* clients;
} client_array;

void init_client_array(client_array* arr, size_t initial_size) {
  arr->allocated = initial_size;
  arr->clients = malloc(initial_size * sizeof(client));
  for (size_t i = 0; i < initial_size; i++) {
    arr->clients[i].active = false;
  }
}

long insert_client(client_array* arr, int queue) {
  long id;
  for (size_t i = 0;; i++) {
    if (i >= arr->allocated) {
      arr->allocated *= 2;
      arr->clients =
          realloc(arr->clients, arr->allocated * sizeof(arr->clients[0]));
      for (size_t j = i; j < arr->allocated; j++) {
        arr->clients[j].active = false;
      }
    }
    if (!arr->clients[i].active) {
      id = i;
      break;
    }
  }
  arr->clients[id] = (client){queue, true, {}};
  return id + 1;
}

void add_friends(client* c, long* friends, size_t count) {
  size_t i = 0;
  while (i < 64 && c->friends[i] != 0) {
    i++;
  }
  for (size_t j = 0; j < count && i < 64; i++, j++) {
    c->friends[i] = friends[j];
  }
}

void del_friends(client* c, long* friends, size_t count) {
  for (size_t i = 0; i < 64 && c->friends[i] != 0; i++) {
    long friend = c->friends[i];
    for (size_t j = 0; j < count; j++) {
      if (friends[j] == friend) {
        for (size_t k = i; k < 64 && c->friends[k] != 0; k++) {
          c->friends[k] = c->friends[k + 1];
        }
      }
    }
  }
}

client* get_client(client_array* arr, long id) {
  if (id > arr->allocated) {
    fprintf(stderr, "Unallocated client id: %ld\n", id);
    return NULL;
  }
  return &arr->clients[id - 1];
}

bool terminating = false;

client_array* g_clients = NULL;

void handleTerm(int sig) {
  terminating = true;
  server_msg msg;
  msg.tag = MSG_TERMINATE;
  for (size_t i = 0; i < g_clients->allocated; i++) {
    client* c = &g_clients->clients[i];
    if (c->active) {
      msg.type = i + 1;
      msgsnd(c->queue, &msg, SERVER_MSG_SIZE, 0);
    }
  }
}

int main(int argc, char** argv) {
  int server_queue = open_queue();

  client_array clients;
  g_clients = &clients;
  init_client_array(&clients, 8);

  sigaction(SIGINT,
            &(struct sigaction){.sa_flags = 0, .sa_handler = handleTerm}, NULL);

  while (true) {
    client_msg req;
    if (terminating) {
      bool done = true;
      for (size_t i = 0; i < clients.allocated; i++) {
        if (clients.clients[i].active) {
          done = false;
          printf("Client %ld still connected\n", i + 1);
          break;
        }
      }
      if (done) {
        msgctl(server_queue, IPC_RMID, NULL);
        exit(0);
      }
    }
    ssize_t res =
        msgrcv(server_queue, &req, CLIENT_MSG_SIZE, -MSG_CLIENT_MAX, 0);
    long sender = req.client_id;
    if (res < 0) {
      if (!terminating) {
        perror("Error while reading from server queue");
      }
      continue;
    }
    printf("Got message type %ld\n", req.type);
    switch (req.type) {
    case MSG_INIT: {
      long id = insert_client(&clients, req.value.init.queue);
      printf("Client %ld connected\n", id);
      server_msg response = {
          .type = id, .tag = MSG_HANDSHAKE, .value = {.handshake = {.id = id}}};
      msgsnd(req.value.init.queue, &response, SERVER_MSG_SIZE, 0);
      break;
    }
    case MSG_STOP: {
      printf("Client %ld disconnecting\n", sender);
      get_client(&clients, sender)->active = false;
      break;
    }
    case MSG_ECHO_REQ: {
      server_msg response = {.type = sender,
                             .tag = MSG_ECHO,
                             .value = {.echo = {.time = time(NULL)}}};
      const size_t response_size = sizeof(response.value.echo.str);
      strncpy(response.value.echo.str, req.value.echo.str, response_size - 1);
      response.value.echo.str[response_size - 1] = 0;
      msgsnd(get_client(&clients, sender)->queue, &response, SERVER_MSG_SIZE,
             0);
      break;
    }
    case MSG_LIST_REQ: {
      server_msg response = {
          .type = sender, .tag = MSG_LIST, .value = {.list = {}}};
      size_t i = 0;
      for (long id = 1; id <= clients.allocated; id++) {
        client* c = get_client(&clients, id);
        if (c->active) {
          response.value.list.clients[i] = id;
          i++;
        }
      }
      response.value.list.count = i;
      msgsnd(get_client(&clients, sender)->queue, &response, SERVER_MSG_SIZE,
             0);
      break;
    }
    case MSG_TOONE: {
      server_msg msg = {.type = req.value.toone.id,
                        .tag = MSG_SAY,
                        .value = {.say = {.from = sender}}};
      strncpy(msg.value.say.str, req.value.toone.str, MSG_MAX_STRING_LENGTH);
      client* client = get_client(&clients, req.value.toone.id);
      if (client == NULL || !client->active) {
        printf("Can't send the message to client %ld\n", req.value.toone.id);
        break;
      }
      msgsnd(client->queue, &msg, SERVER_MSG_SIZE, 0);
      break;
    }
    case MSG_TOALL: {
      server_msg msg = {.tag = MSG_SAY, .value = {.say = {.from = sender}}};
      strncpy(msg.value.say.str, req.value.toall.str, MSG_MAX_STRING_LENGTH);
      for (long addressee = 1; addressee <= clients.allocated; addressee++) {
        if (addressee == sender)
          continue;

        msg.type = addressee;
        client* client = get_client(&clients, addressee);
        if (client->active) {
          msgsnd(client->queue, &msg, SERVER_MSG_SIZE, 0);
        }
      }
      break;
    }
    case MSG_FRIENDS: {
      client* client = get_client(&clients, sender);
      size_t count = req.value.friends.count;
      memcpy(client->friends, req.value.friends.friends, count * sizeof(long));
      if (count < 64) {
        client->friends[count] = 0;
      }
      break;
    }
    case MSG_ADD: {
      add_friends(get_client(&clients, sender), req.value.friends.friends,
                  req.value.friends.count);
      break;
    }
    case MSG_DEL: {
      del_friends(get_client(&clients, sender), req.value.friends.friends,
                  req.value.friends.count);
      break;
    }
    case MSG_TOFRIENDS: {
      server_msg msg = {.tag = MSG_SAY, .value = {.say = {.from = sender}}};
      strncpy(msg.value.say.str, req.value.toall.str, MSG_MAX_STRING_LENGTH);
      client* from = get_client(&clients, sender);
      for (size_t i = 0; i < 64 && from->friends[i] != 0; i++) {
        client* c = get_client(&clients, from->friends[i]);
        if (c != NULL && c->active) {
          msg.type = from->friends[i];
          msgsnd(c->queue, &msg, SERVER_MSG_SIZE, 0);
        }
      }
      break;
    }
    }
  }

  return 0;
}
