#include <stdbool.h>
#include <sys/types.h>

#define MSG_MAX_STRING_LENGTH 64

const char* SERVER_QUEUE_NAME = "/lab06-server";

typedef enum {
  MSG_INIT = 1,
  MSG_STOP,
  MSG_ECHO_REQ,
  MSG_LIST_REQ,
  MSG_FRIENDS,
  MSG_ADD,
  MSG_DEL,
  MSG_TOALL,
  MSG_TOFRIENDS,
  MSG_TOONE,
  MSG_CLIENT_MAX
} client_msg_tag;

typedef struct {
  char queue[64];
} msg_init;

typedef struct {
  long friends[MSG_MAX_STRING_LENGTH];
  size_t count;
} msg_friends;

typedef struct {
  char str[MSG_MAX_STRING_LENGTH];
} msg_string;

typedef struct {
  char str[MSG_MAX_STRING_LENGTH];
  long id;
} msg_toone;

typedef struct {
  long type;
  long client_id;
  union {
    msg_init init;
    msg_string echo;
    msg_friends friends;
    msg_string toall;
    msg_string tofriends;
    msg_toone toone;
  } value;
} client_msg;

typedef enum {
  MSG_HANDSHAKE = 1,
  MSG_ECHO,
  MSG_SAY,
  MSG_LIST,
  MSG_TERMINATE,
  MSG_CLOSE,
  MSG_SERVER_MAX
} server_msg_tag;

typedef struct {
  long id;
} msg_handshake;

typedef struct {
  char str[MSG_MAX_STRING_LENGTH];
  time_t time;
} msg_echo;

typedef struct {
  char str[MSG_MAX_STRING_LENGTH];
  long from;
} msg_say;

typedef struct {
  long clients[MSG_MAX_STRING_LENGTH];
  size_t count;
} msg_list;

typedef struct {
  long type;
  long tag;
  union {
    msg_handshake handshake;
    msg_echo echo;
    msg_say say;
    msg_list list;
  } value;
} server_msg;
