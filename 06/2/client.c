#include <ctype.h>
#include <mqueue.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "msg.h"

mqd_t open_server_queue() {
  mqd_t queue = mq_open(SERVER_QUEUE_NAME, O_WRONLY);
  if (queue < 0) {
    perror("Couldn't open server queue");
    exit(0);
  }
  return queue;
}

mqd_t open_private_queue(char* name, size_t name_size) {
  name[0] = '/';
  name[name_size - 1] = 0;
  srand(time(NULL));
  for (size_t i = 1; i < name_size - 1; i++) {
    name[i] = rand() % ('z' - 'a') + 'a';
  }
  struct mq_attr attr = {.mq_maxmsg = 10, .mq_msgsize = sizeof(server_msg)};
  mqd_t queue = mq_open(name, O_RDONLY | O_CREAT | O_EXCL, 0622, &attr);
  if (queue < 0) {
    perror("Couldn't create private queue");
    exit(-1);
  }
  return queue;
}

size_t parse_friends_list(char* str, long* friends) {
  size_t i = 0;
  while (*str != '\n') {
    long id = strtol(str, &str, 10);
    if (id != 0) {
      friends[i] = id;
      i++;
    }
  }
  return i;
}

mqd_t server_queue_d = -1;
long client_id = -1;

void on_int(int sig) {
  if (server_queue_d != -1 && client_id != -1) {
    client_msg msg;
    msg.type = MSG_STOP;
    msg.client_id = client_id;
    mq_send(server_queue_d, (const char*) &msg, sizeof(msg), 0);
  }
  exit(0);
}

void eval_cmd(char* line, int server_queue, long id, bool* running);

void eval_file(FILE* f, int server_queue, long id, bool* running) {
  char* line = NULL;
  size_t size = 0;
  while (*running) {
    if (getline(&line, &size, f) < 0) {
      return;
    }
    eval_cmd(line, server_queue, id, running);
  }
}

void eval_cmd(char* line, int server_queue, long id, bool* running) {
  client_msg msg;
  msg.client_id = id;
  char* cmd;
  size_t params_start;
  {
    size_t i = 0;
    while (line[i] != 0 && isspace(line[i])) {
      i++;
    }
    size_t start = i;
    while (line[i] != 0 && !isspace(line[i])) {
      i++;
    }
    if (i == start) {
      printf("Illegal command %s\n", line);
      return;
    }
    cmd = malloc(i - start + 1);
    memcpy(cmd, line + start, i - start);
    cmd[i - start] = 0;
    while (line[i] != 0 && isspace(line[i])) {
      i++;
    }
    params_start = i;
  }
  if (strcasecmp("stop", cmd) == 0) {
    msg.type = MSG_STOP;
    *running = false;
  } else if (strcasecmp("echo", cmd) == 0) {
    msg.type = MSG_ECHO_REQ;
    strncpy(msg.value.echo.str, line + params_start, MSG_MAX_STRING_LENGTH - 1);
    msg.value.echo.str[MSG_MAX_STRING_LENGTH - 1] = 0;
  } else if (strcasecmp("list", cmd) == 0) {
    msg.type = MSG_LIST_REQ;
  } else if (strcasecmp("2all", cmd) == 0) {
    msg.type = MSG_TOALL;
    strncpy(msg.value.toall.str, line + params_start,
            MSG_MAX_STRING_LENGTH - 1);
    msg.value.toall.str[MSG_MAX_STRING_LENGTH - 1] = 0;
  } else if (strcasecmp("2friends", cmd) == 0) {
    msg.type = MSG_TOFRIENDS;
    strncpy(msg.value.tofriends.str, line + params_start,
            MSG_MAX_STRING_LENGTH - 1);
    msg.value.tofriends.str[MSG_MAX_STRING_LENGTH - 1] = 0;
  } else if (strcasecmp("2one", cmd) == 0) {
    long addressee;
    char message[MSG_MAX_STRING_LENGTH];
    if (sscanf(line + params_start, "%ld %s", &addressee, message) != 2) {
      fprintf(stderr, "Wrong format of 2one command\n");
      return;
    }
    msg.type = MSG_TOONE;
    msg.value.toone.id = addressee;
    strncpy(msg.value.toone.str, message, MSG_MAX_STRING_LENGTH - 1);
    msg.value.toone.str[MSG_MAX_STRING_LENGTH - 1] = 0;
  } else if (strcasecmp("friends", cmd) == 0) {
    msg.type = MSG_FRIENDS;
    size_t count =
        parse_friends_list(line + params_start, msg.value.friends.friends);
    msg.value.friends.count = count;
  } else if (strcasecmp("add", cmd) == 0) {
    msg.type = MSG_ADD;
    size_t count =
        parse_friends_list(line + params_start, msg.value.friends.friends);
    msg.value.friends.count = count;
  } else if (strcasecmp("del", cmd) == 0) {
    msg.type = MSG_DEL;
    size_t count =
        parse_friends_list(line + params_start, msg.value.friends.friends);
    msg.value.friends.count = count;
  } else if (strcasecmp("read", cmd) == 0) {
    size_t i = params_start;
    for (; line[i] != '\n'; i++)
      ;
    line[i] = 0;
    FILE* f = fopen(line + params_start, "r");
    eval_file(f, server_queue, id, running);
    fclose(f);
  } else {
    fprintf(stderr, "Unknown command\n");
    return;
  }
  mq_send(server_queue, (const char*) &msg, sizeof(msg), 0);
}

void sender(int server_queue, long id) {
  sigaction(SIGINT, &(struct sigaction){.sa_handler = on_int, .sa_flags = 0},
            NULL);

  bool running = true;
  eval_file(stdin, server_queue, id, &running);

  exit(0);
}

int main(int argc, char** argv) {
  mqd_t server = open_server_queue();
  server_queue_d = server;
  char private_name[64];
  mqd_t private = open_private_queue(private_name, sizeof(private_name));
  /* private_queue_id = private; */
  {
    client_msg handshake = {.type = MSG_INIT};
    strncpy(handshake.value.init.queue, private_name, sizeof(handshake.value.init.queue));
    mq_send(server, (const char*) &handshake, sizeof(client_msg), 0);
  }
  long id = -1;
  bool child_started = false;
  pid_t child_pid;
  while (true) {
    server_msg msg;
    ssize_t res = mq_receive(private, (char*) &msg, sizeof(msg), NULL);
    if (res < 0) {
      perror("Error while reading from private queue");
    }
    switch (msg.tag) {
    case MSG_HANDSHAKE: {
      id = msg.value.handshake.id;
      client_id = id;
      printf("Got an id: %ld\n", id);
      if (!child_started) {
        child_started = true;
        pid_t pid = fork();
        if (pid == 0) {
          sender(server, id);
        } else {
          child_pid = pid;
        }
      }
      break;
    }
    case MSG_ECHO: {
      char* time = ctime(&msg.value.echo.time);
      printf("Received echo response: %s - %s\n", time, msg.value.echo.str);
      break;
    }
    case MSG_SAY: {
      printf("%ld: %s\n", msg.value.say.from, msg.value.say.str);
      break;
    }
    case MSG_LIST: {
      printf("Ids on server: ");
      for (size_t i = 0; i < msg.value.list.count; i++) {
        printf("%s%ld", i == 0 ? "" : ", ", msg.value.list.clients[i]);
      }
      printf("\n");
      break;
    }
    case MSG_TERMINATE: {
      if (child_started) {
        sigqueue(child_pid, SIGINT, (union sigval){.sival_int = 0});
      }
      break;
    }
    case MSG_CLOSE: {
      mq_close(server);
      mq_close(private);
      mq_unlink(private_name);
      exit(0);
      break;
    }
    }
  }
  return 0;
}
