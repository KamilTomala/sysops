#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef enum { KILL, SIGQUEUE, SIGRT } mode;

void send(pid_t pid, mode m, int index) {
  switch (m) {
  case KILL:
    kill(pid, SIGUSR1);
    break;
  case SIGQUEUE:
    sigqueue(pid, SIGUSR1, (union sigval){.sival_int = index});
    break;
  case SIGRT:
    sigqueue(pid, SIGRTMIN, (union sigval){.sival_int = index});
    break;
  }
}

void send_end(pid_t pid, mode m) {
  switch (m) {
  case KILL:
    kill(pid, SIGUSR2);
    break;
  case SIGQUEUE:
    sigqueue(pid, SIGUSR2, (union sigval){.sival_int = 0});
    break;
  case SIGRT:
    sigqueue(pid, SIGRTMIN + 1, (union sigval){.sival_int = 0});
    break;
  }
}

pid_t sender_pid = 0;
volatile int received = 0;
volatile bool terminated = false;

void handle_msg(int sig, siginfo_t* info, void* ucontext) {
  sender_pid = info->si_pid;
  received++;
}

void handle_end(int sig, siginfo_t* info, void* ucontext) {
  terminated = true;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("Usage: %s <mode>\n", argv[0]);
    exit(-1);
  }
  mode m;
  if (strcmp(argv[1], "kill") == 0) {
    m = KILL;
  } else if (strcmp(argv[1], "sigqueue") == 0) {
    m = SIGQUEUE;
  } else if (strcmp(argv[1], "sigrt") == 0) {
    m = SIGRT;
    if (SIGRTMIN + 1 > SIGRTMAX) {
      printf("This program needs at least two real time signals to run in rt "
             "mode\n");
      exit(-1);
    }
  } else {
    printf(
        "Wrong mode argument. Must be one of 'kill', 'sigqueue' or 'sigrt'\n");
    exit(-1);
  }
  printf("PID: %d\n", getpid());

  sigaction(
      SIGUSR1,
      &((struct sigaction){.sa_sigaction = handle_msg, .sa_flags = SA_SIGINFO}),
      NULL);
  sigaction(
      SIGUSR2,
      &((struct sigaction){.sa_sigaction = handle_end, .sa_flags = SA_SIGINFO}),
      NULL);
  sigaction(
      SIGRTMIN,
      &((struct sigaction){.sa_sigaction = handle_msg, .sa_flags = SA_SIGINFO}),
      NULL);
  sigaction(
      SIGRTMIN + 1,
      &((struct sigaction){.sa_sigaction = handle_end, .sa_flags = SA_SIGINFO}),
      NULL);

  const int sigs[] = {SIGINT, SIGQUIT, SIGTERM};
  for (size_t i = 0; i < sizeof(sigs) / sizeof(sigs[0]); i++) {
    sigaction(sigs[i], &((struct sigaction){.sa_handler = SIG_IGN}), NULL);
  }

  while (!terminated) {
    pause();
  }
  for (int i = 0; i < received; i++) {
    send(sender_pid, m, i + 1);
  }
  send_end(sender_pid, m);
  printf("Received signals: %d\n", received);

  return 0;
}
