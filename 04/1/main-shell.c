#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

volatile pid_t child = 0;

void start_child() {
  pid_t pid = vfork();
  if (pid == 0) {
    execl("./loop.sh", "loop.sh", NULL);
    perror("Couldn't execute the shell script");
    exit(-1);
  } else {
    child = pid;
  }
}

void kill_child() {
  if (child != 0) {
    kill(child, SIGTERM);
    child = 0;
  }
}

void handle_stop(int i) {
  if (child != 0) {
    kill_child();
    printf("Oczekuję na CTRL+Z - kontynuacja albo CTRL+C - zakończnie "
           "programu.\n");
  } else {
    start_child();
  }
}

void handle_int(int i) {
  printf("Odebrano sygnał SIGINT\n");
  kill_child();
  exit(0);
}

int main(int argc, char** argv) {
  {
    struct sigaction sa;
    sa.sa_handler = handle_stop;
    sigaction(SIGTSTP, &sa, NULL);
  }
  signal(SIGINT, handle_int);

  start_child();

  while (true) {
    pause();
  }

  return 0;
}
