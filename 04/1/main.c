#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

volatile bool running = true;

void handle_stop(int i) {
  if (running) {
    printf("Oczekuję na CTRL+Z - kontynuacja albo CTRL+C - zakończnie "
           "programu.\n");
  }
  running = !running;
}

void handle_int(int i) {
  printf("Odebrano sygnał SIGINT\n");
  exit(0);
}

int main(int argc, char** argv) {
  {
    struct sigaction sa;
    sa.sa_handler = handle_stop;
    sigaction(SIGTSTP, &sa, NULL);
  }
  signal(SIGINT, handle_int);

  while (true) {
    while (running) {
      const size_t BUFF_SIZE = 32;
      char buff[BUFF_SIZE];
      time_t now = time(NULL);
      struct tm* t = localtime(&now);
      strftime(buff, BUFF_SIZE, "%Y-%m-%d_%H-%M-%S", t);
      printf("%s\n", buff);
    }
    while (!running) {
      pause();
    }
  }

  return 0;
}
