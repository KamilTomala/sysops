#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef enum { KILL, SIGQUEUE, SIGRT } mode;

volatile bool ack = false;

void send(pid_t pid, mode m) {
  ack = false;
  switch (m) {
  case KILL:
    kill(pid, SIGUSR1);
    break;
  case SIGQUEUE:
    sigqueue(pid, SIGUSR1, (union sigval){.sival_int = 0});
    break;
  case SIGRT:
    sigqueue(pid, SIGRTMIN, (union sigval){.sival_int = 0});
    break;
  }
}

void send_end(pid_t pid, mode m) {
  switch (m) {
  case KILL:
    kill(pid, SIGUSR2);
    break;
  case SIGQUEUE:
    sigqueue(pid, SIGUSR2, (union sigval){.sival_int = 0});
    break;
  case SIGRT:
    sigqueue(pid, SIGRTMIN + 1, (union sigval){.sival_int = 0});
    break;
  }
}

volatile int received = 0;
volatile bool terminated = false;
volatile int received_index = -1;

void handle_ack(int sig) {
  ack = true;
}

void handle_msg(int sig, siginfo_t* info, void* ucontext) {
  received++;
  received_index = info->si_value.sival_int;
}

void handle_end(int sig, siginfo_t* info, void* ucontext) {
  terminated = true;
}

int main(int argc, char** argv) {
  if (argc < 4) {
    printf("Usage: %s <n> <pid> <mode>\n", argv[0]);
    exit(-1);
  }
  char* end;
  unsigned long n = strtoul(argv[1], &end, 10);
  if (*end != 0) {
    printf("First argument has to be a number\n");
    exit(-1);
  }
  pid_t pid = strtoul(argv[2], &end, 10);
  if (*end != 0) {
    printf("Second argument has to be a number\n");
    exit(-1);
  }
  mode m;
  if (strcmp(argv[3], "kill") == 0) {
    m = KILL;
  } else if (strcmp(argv[3], "sigqueue") == 0) {
    m = SIGQUEUE;
  } else if (strcmp(argv[3], "sigrt") == 0) {
    m = SIGRT;
    if (SIGRTMIN + 1 > SIGRTMAX) {
      printf("This program needs at least two real time signals to run in rt "
             "mode\n");
      exit(-1);
    }
  } else {
    printf(
        "Wrong mode argument. Must be one of 'kill', 'sigqueue' or 'sigrt'\n");
    exit(-1);
  }
  sigaction(SIGUSR1, &((struct sigaction){.sa_handler = handle_ack}), NULL);

  for (unsigned long i = 0; i < n; i++) {
    send(pid, m);
    while (!ack) {
      pause();
    }
  }
  send_end(pid, m);

  sigaction(
      SIGUSR1,
      &((struct sigaction){.sa_sigaction = handle_msg, .sa_flags = SA_SIGINFO}),
      NULL);
  sigaction(
      SIGUSR2,
      &((struct sigaction){.sa_sigaction = handle_end, .sa_flags = SA_SIGINFO}),
      NULL);
  sigaction(
      SIGRTMIN,
      &((struct sigaction){.sa_sigaction = handle_msg, .sa_flags = SA_SIGINFO}),
      NULL);
  sigaction(
      SIGRTMIN + 1,
      &((struct sigaction){.sa_sigaction = handle_end, .sa_flags = SA_SIGINFO}),
      NULL);

  while (!terminated) {
    pause();
  }
  printf("Received signals: %d / %d / %lu\n", received, received_index, n);

  return 0;
}
