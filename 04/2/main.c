#include <ctype.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <wait.h>

typedef struct {
  char* path;
  int interval;
} file_entry;

typedef struct {
  size_t size;
  file_entry entries[];
} file_array;

typedef struct {
  char* buffer;
  size_t allocated;
  size_t used;
} mem_buffer;

size_t count_lines(FILE* f) {
  size_t lines = 0;
  char c;
  bool newline_last = false;
  while ((c = fgetc(f)) != EOF) {
    if (c == '\n') {
      lines++;
      newline_last = true;
    } else {
      newline_last = false;
    }
  }
  if (!newline_last) {
    lines++;
  }
  return lines;
}

file_array* alloc_file_array(size_t size) {
  file_array* ret = calloc(sizeof(file_array) + size * sizeof(file_entry), 1);
  ret->size = size;
  return ret;
}

void parse_line(char* line, file_entry* entry) {
  size_t i = 0;
  // Skip any whitespace at the beginning
  while (isspace(line[i])) {
    i++;
  }

  size_t path_start;
  size_t path_end;

  if (line[i] == '"') {
    i++;
    path_start = i;
    while (line[i] != '"') {
      i++;
    }
    path_end = i;
    i++;
  } else {
    path_start = i;
    while (!isspace(line[i])) {
      i++;
    }
    path_end = i;
  }

  char* path = calloc(1, path_end - path_start + 1);
  memcpy(path, line + path_start, path_end - path_start);

  while (isspace(line[i]))
    i++;

  int interval;
  sscanf(line + i, "%u", &interval);

  entry->path = path;
  entry->interval = interval;
}

file_array* parse_file(char* path) {
  FILE* f = fopen(path, "r");
  if (f == NULL) {
    perror("Couldn't open the list file");
    return NULL;
  }
  size_t lines = count_lines(f);

  file_array* arr = alloc_file_array(lines);

  char* line = NULL;
  size_t line_size = 0;

  fseek(f, 0, SEEK_SET);
  for (size_t i = 0; i < lines; i++) {
    getline(&line, &line_size, f);
    parse_line(line, &arr->entries[i]);
  }
  if (line != NULL) {
    free(line);
  }

  return arr;
}

time_t get_modification_date(char* path) {
  struct stat res;
  lstat(path, &res);
  return res.st_mtim.tv_sec;
}

char* make_backup_path(char* path, time_t mod) {
  const char* const prefix = "archiwum/";
  const size_t prefix_size = strlen(prefix);
  size_t len = strlen(path);

  size_t date_size = 21;
  char* date_str = calloc(1, date_size);

  struct tm* t = localtime(&mod);
  strftime(date_str, date_size, "_%Y-%m-%d_%H-%M-%S", t);

  size_t ep_len = len + prefix_size + date_size + 1;
  char* escaped_path = calloc(1, ep_len);
  snprintf(escaped_path, ep_len, "%s%s%s", prefix, path, date_str);
  for (size_t i = 0; i < len; i++) {
    if (escaped_path[prefix_size + i] == '/') {
      escaped_path[prefix_size + i] = '!';
    }
  }
  return escaped_path;
}

void read_to_mem(char* path, mem_buffer* buffer) {
  FILE* f = fopen(path, "r");
  fseek(f, 0, SEEK_END);
  size_t fsize = ftell(f);
  fseek(f, 0, SEEK_SET);

  if (buffer->buffer == NULL) {
    buffer->buffer = calloc(1, fsize);
    buffer->allocated = fsize;
  } else if (buffer->allocated < fsize) {
    buffer->buffer = realloc(buffer->buffer, fsize);
    buffer->allocated = fsize;
  }

  fread(buffer->buffer, 1, fsize, f);
  buffer->used = fsize;

  fclose(f);
}

void backup_mem(char* path, time_t mod, mem_buffer* buffer) {
  char* escaped_path = make_backup_path(path, mod);

  FILE* f = fopen(escaped_path, "w");
  fwrite(buffer->buffer, 1, buffer->used, f);
  fclose(f);

  read_to_mem(path, buffer);
}

bool paused = false;

void handle_stop(int sig) {
  if (!paused) {
    printf("Process %d is stopped\n", getpid());
  }
  paused = true;
}

void handle_start(int sig) {
  if (paused) {
    printf("Process %d is started\n", getpid());
  }
  paused = false;
}

void monitor(file_entry* entry) {
  {
    struct sigaction sa;
    sa.sa_handler = handle_stop;
    sigaction(SIGUSR1, &sa, NULL);
    sa.sa_handler = handle_start;
    sigaction(SIGUSR2, &sa, NULL);
  }
  struct timeval start_time;
  gettimeofday(&start_time, NULL);

  time_t last_modified = get_modification_date(entry->path);

  int copies_made = 0;
  mem_buffer buffer = {.buffer = NULL, .allocated = 0, .used = 0};

  read_to_mem(entry->path, &buffer);

  while (true) {
    while (paused) {
      pause();
    }
    time_t mod = get_modification_date(entry->path);
    if (mod > last_modified) {
      last_modified = mod;
      backup_mem(entry->path, last_modified, &buffer);
      copies_made++;
      printf("File %s modified\n", entry->path);
    }

    sleep(entry->interval);

    struct timeval now;
    gettimeofday(&now, NULL);
  }
  exit(copies_made);
}

void list_children(file_array* arr, pid_t* pids) {
  for (size_t i = 0; i < arr->size; i++) {
    printf("%d - %s\n", pids[i], arr->entries[i].path);
  }
}

bool parent_terminated = false;

void handle_parent_term(int sig) {
  parent_terminated = true;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    fprintf(stderr, "Too few arguments\n");
    return -1;
  }

  {
    struct sigaction sa;
    sa.sa_handler = handle_parent_term;
    sigaction(SIGINT, &sa, NULL);
  }

  file_array* arr = parse_file(argv[1]);
  pid_t* child_pids = calloc(sizeof(pid_t), arr->size);
  if (arr == NULL) {
    return -1;
  }
  for (size_t i = 0; i < arr->size; i++) {
    pid_t pid = fork();
    if (pid == 0) {
      monitor(&arr->entries[i]);
    } else {
      child_pids[i] = pid;
    }
  }

  list_children(arr, child_pids);

  char* line = NULL;
  size_t line_size = 0;
  while (!parent_terminated) {
    ssize_t line_chars = getline(&line, &line_size, stdin);
    if (line_chars <= 0 || line == NULL)
      continue;
    for (ssize_t i = 0; i < line_chars; i++) {
      if (line[i] >= 'A' && line[i] <= 'Z') {
        line[i] += 'a' - 'A';
      }
    }
    const size_t MAX_CMD_SIZE = 8;
    char cmd[MAX_CMD_SIZE + 1];
    size_t argument_start;
    {
      size_t i = 0;
      while (i < line_chars && isspace(line[i])) {
        i++;
      }
      size_t j = 0;
      while (j < MAX_CMD_SIZE && i < line_chars && !isspace(line[i])) {
        cmd[j] = line[i];
        i++;
        j++;
      }
      cmd[j + 1] = 0;
      while (i < line_chars && isspace(line[i])) {
        i++;
      }
      argument_start = i;
    }
    if (strcmp(cmd, "list") == 0) {
      list_children(arr, child_pids);
    } else if (strcmp(cmd, "stop") == 0) {
      if (strcmp(&line[argument_start], "all\n") == 0) {
        for (size_t i = 0; i < arr->size; i++) {
          kill(child_pids[i], SIGUSR1);
        }
      } else {
        char* end;
        pid_t pid = strtol(&line[argument_start], &end, 10);
        if (*end != 0 && *end != '\n') {
          fprintf(
              stderr,
              "Wrong argument to stop command, should be 'all' or a pid.\n");
        } else {
          kill(pid, SIGUSR1);
        }
      }
    } else if (strcmp(cmd, "start") == 0) {
      if (strcmp(&line[argument_start], "all\n") == 0) {
        for (size_t i = 0; i < arr->size; i++) {
          kill(child_pids[i], SIGUSR2);
        }
      } else {
        char* end;
        pid_t pid = strtol(&line[argument_start], &end, 10);
        if (*end != 0 && *end != '\n') {
          fprintf(
              stderr,
              "Wrong argument to start command, should be 'all' or a pid.\n");
        } else {
          kill(pid, SIGUSR2);
        }
      }
    } else if (strcmp(cmd, "end") == 0) {
      break;
    }
  }

  for (size_t i = 0; i < arr->size; i++) {
    kill(child_pids[i], SIGTERM);
    int status;
    waitpid(child_pids[i], &status, 0);
    printf("Process %d made %d copies\n", child_pids[i], WEXITSTATUS(status));
  }

  return 0;
}
