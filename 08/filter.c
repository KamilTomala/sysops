#include <ctype.h>
#include <math.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef enum { MODE_BLOCK, MODE_INTERLEAVED } distr_mode;

typedef struct {
  size_t w;
  size_t h;
  uint8_t pixels[];
} image;

typedef struct {
  size_t size;
  double coef[];
} kernel;

image* alloc_image(size_t w, size_t h) {
  image* img = malloc(sizeof(image) + w * h * sizeof(uint8_t));
  img->w = w;
  img->h = h;
  return img;
}

bool skip_comment(FILE* f, char** str, char** line, size_t* str_size) {
  char* c = *str;
  while (true) {
    while (isspace(*c)) {
      c++;
    }
    if (*c == '#') {
      while (*c != 0 && *c != '\n' && *c != '\r') {
        c++;
      }
      while (*c == '\n' || *c == '\r') {
        c++;
      }
    }
    if (*c == 0) {
      if (getline(line, str_size, f) < 0) {
        return true;
      }
      c = *line;
    } else {
      *str = c;
      return false;
    }
  }
}

image* read_image(const char* path) {
  FILE* f = fopen(path, "r");

  char* line = NULL;
  size_t line_size = 0;

  if (getline(&line, &line_size, f) < 0) {
    perror("Couldn't read the first line of image");
    exit(EXIT_FAILURE);
  }

  char* read = line;

  if (skip_comment(f, &read, &line, &line_size)) {
    fprintf(stderr, "Couldn't read the header\n");
  }
  if (read[0] != 'P' || read[1] != '2') {
    fprintf(stderr, "Magic number not recognized: %2s\n", read);
  }
  read += 2;
  if (skip_comment(f, &read, &line, &line_size)) {
    fprintf(stderr, "Couldn't read the header\n");
  }
  size_t w = strtol(read, &read, 10);
  if (skip_comment(f, &read, &line, &line_size)) {
    fprintf(stderr, "Couldn't read the header\n");
  }
  size_t h = strtol(read, &read, 10);
  if (skip_comment(f, &read, &line, &line_size)) {
    fprintf(stderr, "Couldn't read the header\n");
  }
  int d = strtol(read, &read, 10);
  if (skip_comment(f, &read, &line, &line_size)) {
    fprintf(stderr, "Couldn't read the header\n");
  }
  if (d != 255) {
    fprintf(stderr, "Unsupported max pixel value: %d\n", d);
    exit(EXIT_FAILURE);
  }
  image* img = alloc_image(w, h);

  size_t i = 0;

  while (!skip_comment(f, &read, &line, &line_size)) {
    long pixel = strtol(read, &read, 10);
    if (pixel > d) {
      fprintf(stderr, "Pixel value greater than the maximum: %ld\n", pixel);
      pixel = 255;
    }
    img->pixels[i] = (uint8_t) pixel;
    i++;
    if (i >= w * h) {
      break;
    }
  }

  if (i < w * h) {
    fprintf(stderr, "Wrong number of pixels in file, expected %zd, found %zd",
            i, w * h);
    exit(EXIT_FAILURE);
  }

  fclose(f);
  return img;
}

void write_image(const char* path, image* img) {
  FILE* f = fopen(path, "w");
  fprintf(f, "P2\n%zd %zd\n255", img->w, img->h);
  for (size_t i = 0; i < img->w * img->h; i++) {
    if (i % 20 == 0) {
      fprintf(f, "\n");
    }
    fprintf(f, "%hhu ", img->pixels[i]);
  }
}

kernel* read_kernel(const char* path) {
  FILE* f = fopen(path, "r");
  if (f == NULL) {
    perror("Couldn't open the kernel file");
    exit(EXIT_FAILURE);
  }

  size_t c;
  if (fscanf(f, " %zd ", &c) != 1) {
    fprintf(stderr, "Couldn't read the kernel size\n");
    exit(EXIT_FAILURE);
  }

  kernel* ret = malloc(sizeof(*ret) + c * c * sizeof(ret->coef[0]));
  ret->size = c;

  for (size_t i = 0; i < c * c; i++) {
    if (fscanf(f, " %lf ", ret->coef + i) != 1) {
      fprintf(stderr, "Couldn't read a kernel coefficient\n");
      exit(EXIT_FAILURE);
    }
  }

  return ret;
}

uint8_t image_get(const image* img, ssize_t x, ssize_t y) {
  x = x >= ((ssize_t) img->w) ? img->w - 1 : x < 0 ? 0 : x;
  y = y >= ((ssize_t) img->h) ? img->h - 1 : y < 0 ? 0 : y;
  return img->pixels[y * img->w + x];
}

void image_set(image* img, ssize_t x, ssize_t y, uint8_t pixel) {
  if (x >= 0 && x < img->w && y >= 0 && y < img->h) {
    img->pixels[y * img->w + x] = pixel;
  }
}

double kernel_get(const kernel* ker, size_t x, size_t y) {
  x = x >= ker->size ? ker->size - 1 : x < 0 ? 0 : x;
  y = y >= ker->size ? ker->size - 1 : y < 0 ? 0 : y;
  return ker->coef[y * ker->size + x];
}

uint8_t compute_pixel(const image* img, const kernel* k, size_t x, size_t y) {
  double v = 0.0;
  ssize_t bias = k->size / 2 + (1 - k->size % 2);
  for (size_t i = 0; i < k->size; i++) {
    for (size_t j = 0; j < k->size; j++) {
      v += ((double) kernel_get(k, i, j)) *
           ((double) image_get(img, ((ssize_t) x) - bias + i,
                               ((ssize_t) y) - bias + j));
    }
  }
  return v < 0.0 ? 0 : v > 255.0 ? 255 : (uint8_t) round(v);
}

void compute_column(const image* in, const kernel* ker, image* out, size_t x) {
  for (size_t y = 0; y < out->h; y++) {
    image_set(out, x, y, compute_pixel(in, ker, x, y));
  }
}

void compute_block(const image* in, const kernel* ker, image* out, size_t start,
                   size_t stride, size_t count) {
  for (size_t i = 0, x = start; i < count && x < out->w; i++, x += stride) {
    compute_column(in, ker, out, x);
  }
}

typedef struct {
  int id;
  const image* in;
  const kernel* ker;
  image* out;
  size_t start;
  size_t stride;
  size_t count;
} block_args;

typedef struct {
  int id;
  long time;
} thread_result;

void* spawn_block(void* a) {
  block_args* args = a;
  struct timespec start;
  clock_gettime(CLOCK_MONOTONIC, &start);

  compute_block(args->in, args->ker, args->out, args->start, args->stride,
                args->count);

  struct timespec end;
  clock_gettime(CLOCK_MONOTONIC, &end);
  long elapsed = (end.tv_sec - start.tv_sec) * 1000 * 1000 +
                 (end.tv_nsec - start.tv_nsec) / 1000;
  thread_result* ret = malloc(sizeof(*ret));
  ret->id = args->id;
  ret->time = elapsed;
  free(args);
  return ret;
}

int main(int argc, char** argv) {
  if (argc != 6) {
    printf("Usage: %s <thread_count> <block|interleaved> <in_file> "
           "<filter_file> <out_file>\n",
           argv[0]);
    exit(EXIT_FAILURE);
  }
  char* tc_end;
  int thread_count = strtol(argv[1], &tc_end, 10);
  if (*tc_end != 0) {
    printf("Not a number: '%s'\n", argv[1]);
  }

  distr_mode mode;
  if (strcasecmp(argv[2], "block") == 0) {
    mode = MODE_BLOCK;
  } else if (strcasecmp(argv[2], "interleaved") == 0) {
    mode = MODE_INTERLEAVED;
  } else {
    printf("'%s' is not a valid mode.\n", argv[2]);
    exit(EXIT_FAILURE);
  }

  char* in_file = argv[3];
  char* filter_file = argv[4];
  char* out_file = argv[5];

  image* img = read_image(in_file);

  kernel* ker = read_kernel(filter_file);

  image* out_img = alloc_image(img->w, img->h);

  pthread_t* threads = calloc(thread_count, sizeof(threads[0]));

  struct timespec start;
  clock_gettime(CLOCK_MONOTONIC, &start);

  for (int i = 0; i < thread_count; i++) {
    block_args* args = malloc(sizeof(*args));

    args->id = i;
    args->in = img;
    args->ker = ker;
    args->out = out_img;

    switch (mode) {
    case MODE_BLOCK: {
      size_t size = img->w / thread_count;
      if (img->w % thread_count > thread_count / 2) {
        size++;
      }
      args->start = i * size;
      args->stride = 1;
      args->count = size;
      break;
    }
    case MODE_INTERLEAVED:
      args->start = i;
      args->stride = thread_count;
      args->count = img->w; // The threads will stop once they go beyond the
                            // image's right border
      break;
    }

    pthread_create(threads + i, NULL, spawn_block, args);
  }

  for (int i = 0; i < thread_count; i++) {
    thread_result* ret;
    pthread_join(threads[i], (void*) &ret);
    fprintf(stderr, "%2d: %10ld us\n", ret->id, ret->time);
    free(ret);
  }

  struct timespec end;
  clock_gettime(CLOCK_MONOTONIC, &end);
  long elapsed = (end.tv_sec - start.tv_sec) * 1000 * 1000 +
                 (end.tv_nsec - start.tv_nsec) / 1000;

  printf("%ld\n", elapsed);

  write_image(out_file, out_img);

  return EXIT_SUCCESS;
}
