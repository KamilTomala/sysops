c = ARGV[0].to_i

arr = (0...c).map do
  (0...c).map do
    rand() - 0.5
  end
end

puts "#{c}"
sum = arr.map{ |r| r.sum }.sum
c.times do |y|
  c.times do |x|
    print "#{arr[y][x] / sum} "
  end
  puts()
end
