kernels = ["rand10", "rand20", "rand50", "rand100"]
thread_counts = [1, 2, 4, 8, 16]
modes = ["block", "interleaved"]
in_file = "totem.ascii.pgm"

puts "kernel,thread_count,mode,time"
kernels.each do |kernel|
  thread_counts.each do |thread_count|
    modes.each do |mode|
      out = `out/filter #{thread_count} #{mode} #{in_file} kernel/#{kernel} /dev/null`
      puts "#{kernel},#{thread_count},#{mode},#{out}"
    end
  end
end
