d = ARGV[0].to_i

arr = []
d.times do |y|
  row = []
  d.times do |x|
    inside = (x - d / 2) ** 2 + (y - d / 2) ** 2 <= d * d / 4
    row.push(inside ? 1.0 : 0.0)
  end
  arr.push row
end

puts "#{d}"
sum = arr.map{ |r| r.sum }.sum
d.times do |y|
  d.times do |x|
    print "#{arr[y][x] / sum} "
  end
  puts()
end
