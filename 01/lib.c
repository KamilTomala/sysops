#include <stdio.h>
#include <stdlib.h>

#include "lib.h"

static const char* search_directory;
static const char* search_query;

block_arr* create_array(size_t size) {
  block_arr* arr = calloc(1, sizeof(block_arr) + size * sizeof(char*));
  if (arr == NULL) {
    return NULL;
  }
  arr->size = size;
  return arr;
}

void set_search_directory(const char* str) {
  search_directory = str;
}

void set_search_query(const char* str) {
  search_query = str;
}

int execute_search(const char* temp_file) {
  const size_t CMD_SIZE = 255;
  char cmd[CMD_SIZE];
  size_t printf_res = snprintf(cmd, CMD_SIZE, "find %s -name \"%s\" > %s",
                               search_directory, search_query, temp_file);
  if (printf_res <= 0 || printf_res >= CMD_SIZE) {
    fprintf(stderr, "Error while preparing find command string\n");
    return -1;
  }
  if (system((const char*) &cmd)) {
    fprintf(stderr, "Error while executing the find command\n");
  }
  return 0;
}

static size_t allocate_block(block_arr* arr, size_t size) {
  for (size_t i = 0; i < arr->size; i++) {
    if (arr->elements[i] == NULL) {
      char* block = calloc(size, 1);
      if (block == NULL) {
        fprintf(stderr, "Error while allocating a memory block for result\n");
        return -1;
      }
      arr->elements[i] = block;
      return i;
    }
  }
  fprintf(stderr, "No free elements left in the block array\n");
  return -2;
}

int read_result(block_arr* arr, const char* temp_file) {
  FILE* f = fopen(temp_file, "r");

  // Seek to the end and get position
  fseek(f, 0L, SEEK_END);
  size_t size = ftell(f);
  // Seek back to the beginning of the file
  rewind(f);

  size_t index = allocate_block(arr, size + 1);
  if (index < 0) {
    return -1;
  }

  // Load the file contents
  if (fread(arr->elements[index], 1, size, f) != size) {
    fprintf(stderr, "Error while reading the temporary file\n");
  }
  arr->elements[index][size] = 0;

  return index;
}

void free_block(block_arr* arr, size_t index) {
  if (arr->elements[index] != NULL) {
    free(arr->elements[index]);
    arr->elements[index] = NULL;
  }
}

void free_all_blocks(block_arr* arr) {
  for (size_t i = 0; i < arr->size; i++) {
    free_block(arr, i);
  }
}
