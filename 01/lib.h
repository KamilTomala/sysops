#pragma once

#include <stdlib.h>

typedef struct block_arr {
  size_t size;
  char* elements[];
} block_arr;

#ifndef DYNAMIC
block_arr* create_array(size_t size);

void set_search_directory(const char* str);

void set_search_query(const char* str);

int execute_search(const char* temp_file);

int read_result(block_arr* arr, const char* temp_file);

void free_block(block_arr* arr, size_t index);

void free_all_blocks(block_arr* arr);
#endif
