#include "lib.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/times.h>
#include <unistd.h>

#include <dlfcn.h>

#ifdef DYNAMIC
block_arr* (*create_array)(size_t size);

void (*set_search_directory)(const char* str);

void (*set_search_query)(const char* str);

int (*execute_search)(const char* temp_file);

int (*read_result)(block_arr* arr, const char* temp_file);

void (*free_block)(block_arr* arr, size_t index);

void (*free_all_blocks)(block_arr* arr);
#endif

typedef struct measurement {
  char* cmd;
  unsigned real;
  unsigned user;
  unsigned sys;
  struct measurement* next;
} measurement;

typedef struct context {
  char** argv;
  size_t argc;
  size_t i;
  block_arr* blocks;
  char* temp_file;
  measurement* first_measurement;
  measurement* last_measurement;
} context;

char* pop(context* ctxt) {
  if (ctxt->i >= ctxt->argc) {
    return NULL;
  }
  return ctxt->argv[ctxt->i++];
}

void add_measurement(context* ctxt, char* cmd, unsigned real, unsigned user,
                     unsigned sys) {
  measurement* m = malloc(sizeof(*m));
  m->cmd = cmd;
  m->real = real;
  m->user = user;
  m->sys = sys;
  m->next = NULL;
  if (ctxt->last_measurement != NULL) {
    ctxt->last_measurement->next = m;
  }
  ctxt->last_measurement = m;
  if (ctxt->first_measurement == NULL) {
    ctxt->first_measurement = m;
  }
}

typedef int (*command)(context*);

int cmd_create_table(context* ctxt) {
  size_t size;
  char* str_end = NULL;
  char* size_arg = pop(ctxt);
  if (size_arg == NULL)
    return -1;
  size = strtol(size_arg, &str_end, 10);
  if (*str_end != 0) {
    fprintf(stderr, "Argument of create_table has to be a number\n");
    return -1;
  }
  if (ctxt->blocks != NULL) {
    free_all_blocks(ctxt->blocks);
  }
  ctxt->blocks = create_array(size);
  return 0;
}

int cmd_search_directory(context* ctxt) {
  char* dir = pop(ctxt);
  char* file = pop(ctxt);
  char* temp = pop(ctxt);
  if (temp == NULL) {
    fprintf(stderr,
            "Not enough arguments passed to search_directory command\n");
    return -1;
  }
  set_search_directory(dir);
  set_search_query(file);
  ctxt->temp_file = temp;
  return execute_search(temp);
}

int cmd_read_results(context* ctxt) {
  size_t index = read_result(ctxt->blocks, ctxt->temp_file);
  printf("Loaded result at index %lu\n", index);
  return 0;
}

int cmd_remove_block(context* ctxt) {
  size_t index;
  char* str_end = NULL;
  char* size_arg = pop(ctxt);
  if (size_arg == NULL)
    return -1;
  index = strtol(size_arg, &str_end, 10);
  if (*str_end != 0) {
    fprintf(stderr, "Argument of remove_block has to be a number\n");
    return -1;
  }
  free_block(ctxt->blocks, index);
  return 0;
}

int cmd_print_block(context* ctxt) {
  size_t index;
  char* str_end = NULL;
  char* size_arg = pop(ctxt);
  if (size_arg == NULL)
    return -1;
  index = strtol(size_arg, &str_end, 10);
  if (*str_end != 0) {
    fprintf(stderr, "Argument of print_block has to be a number\n");
    return -1;
  }
  char* block = ctxt->blocks->elements[index];
  printf("Block %lu:\n", index);
  if (block == NULL) {
    printf("<empty>\n");
  } else {
    printf("%s\n", block);
  }
  return 0;
}

char* command_names[] = {"create_table", "search_directory", "read_results",
                         "remove_block", "print_block"};

command commands[] = {&cmd_create_table, &cmd_search_directory,
                      &cmd_read_results, &cmd_remove_block, &cmd_print_block};

void printUsage() {
  printf("Available commands:\n");
  printf(" create_table <size>\n");
  printf(" search_directory <dir> <file> <temp>\n");
  printf(" read_results\n");
  printf(" remove_block <i>\n");
  printf(" print_block <i>\n");
}

void fprint_measurement(FILE* f, measurement* m, unsigned tps) {
  fprintf(f, "%20s real %5dms user %5dms system %5dms\n", m->cmd,
          m->real * 1000 / tps, m->user * 1000 / tps, m->sys * 1000 / tps);
}

void print_report(measurement* m) {
  unsigned tps = sysconf(_SC_CLK_TCK);
  FILE* report_file = fopen("report", "w");
  for (measurement* i = m; i != NULL; i = i->next) {
    fprint_measurement(stdout, i, tps);
    fprint_measurement(report_file, i, tps);
  }
  fclose(report_file);
}

int main(int argc, char** argv) {
#ifdef DYNAMIC
  void* lib_handle = dlopen("./libfind.so", RTLD_LAZY);
  if (lib_handle == NULL) {
    fprintf(stderr, "%s\n", dlerror());
    return -1;
  }
  create_array = dlsym(lib_handle, "create_array");
  set_search_directory = dlsym(lib_handle, "set_search_directory");
  set_search_query = dlsym(lib_handle, "set_search_query");
  execute_search = dlsym(lib_handle, "execute_search");
  read_result = dlsym(lib_handle, "read_result");
  free_block = dlsym(lib_handle, "free_block");
  free_all_blocks = dlsym(lib_handle, "free_all_blocks");
#endif
  if (argc <= 1) {
    printUsage();
    return 0;
  }
  context ctxt = {.argv = argv,
                  .argc = argc,
                  .i = 1,
                  .blocks = NULL,
                  .temp_file = NULL,
                  .first_measurement = NULL,
                  .last_measurement = NULL};
  char* cmd = NULL;
  while ((cmd = pop(&ctxt)) != NULL) {
    bool found = false;
    for (size_t i = 0; i < sizeof(command_names) / sizeof(command_names[0]);
         i++) {
      if (strcmp(cmd, command_names[i]) == 0) {
        struct tms tms_before;
        clock_t clock_before = times(&tms_before);

        int res = commands[i](&ctxt);

        struct tms tms_after;
        clock_t clock_after = times(&tms_after);

        add_measurement(&ctxt, cmd, clock_after - clock_before,
                        tms_after.tms_utime - tms_before.tms_utime,
                        tms_after.tms_stime - tms_before.tms_stime);

        if (res) {
          print_report(ctxt.first_measurement);
          return -1;
        }
        found = true;
        break;
      }
    }
    if (!found) {
      fprintf(stderr, "Unknown command '%s', aborting.\n", cmd);
      printUsage();
    }
  }
  print_report(ctxt.first_measurement);
  return 0;
}
